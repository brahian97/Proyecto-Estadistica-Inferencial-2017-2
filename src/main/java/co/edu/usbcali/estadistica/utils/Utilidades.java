package co.edu.usbcali.estadistica.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.commons.math3.distribution.FDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.TDistribution;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utilidades {

	private final static Logger log = LoggerFactory.getLogger(Utilidades.class);

	public static List<Double> ordenarAsc(List<Double> datos) {
		boolean swapped = true;
		int j = 0;
		Double tmp;
		while (swapped) {
			swapped = false;
			j++;
			for (int i = 0; i < datos.size() - j; i++) {
				if (datos.get(i) > datos.get(i + 1)) {
					tmp = (Double) datos.get(i);
					datos.set(i, datos.get(i + 1));
					datos.set(i + 1, tmp);
					swapped = true;
				}
			}
		}
		return datos;
	}
	
	public static List<String> obtenerDatosString(String strDatos) throws Exception{
		if(strDatos.isEmpty()) {
			throw new Exception("Ingrese datos");
		}
		try {
			List<String> datos = new ArrayList<String>();
			String arr[] = strDatos.split(",");
			for(int i=0; i<arr.length; i++) {
				arr[i]=arr[i].trim();
				datos.add(arr[i]);
				log.info(arr[i]);
			}
			return datos;
		}catch (Exception e) {
			throw new Exception("Error obteniendo los datos");
		}
	}
	
	public static double[] stringToDoubleAray(String strDatos) throws Exception {
		try {
			List<Double> lista = obtenerDatosDouble(strDatos);
			double[] array = new double[lista.size()];
			int cont = 0;
			for(int i=0; i<lista.size(); i++) {
				log.info(""+lista.get(i));
				array[cont] = lista.get(i).doubleValue();
			}
			return array;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public static List<Double> obtenerDatosDouble(String strDatos) throws Exception{
		try {
			List<String> datosString = obtenerDatosString(strDatos);
			List<Double> datos = new ArrayList<Double>();
			for(int i=0; i<datosString.size(); i++) {
				datos.add(Double.parseDouble(datosString.get(i)));
			}
			return datos;
		}catch (Exception e) {
			throw new Exception("Los datos no son numéricos: "+e.getMessage());
		}
	}
	
	public static Double convertirtxtValorDouble(String valor)throws Exception {
		if(valor.trim().isEmpty()) {
			throw new Exception("Falta uno o varios datos por ingresar");
		}
		try {
			Double valorD=0.0;
			valorD=Double.parseDouble(valor.trim());
			log.info("valor: "+valorD);
			return valorD;
		} catch (Exception e) {
			throw new Exception("El valor "+valor+" no es numerico");
		}
	}
	
	public static int convertirStringToInt(String dato) throws Exception{
		log.info("--------------------------");
		if(dato.isEmpty()) {
			throw new Exception("ingrese todo los datos");
		}
		try {
			int valor = 0;
			valor = Integer.parseInt(dato);
			return valor;
		}catch (Exception e) {
			throw new Exception("El valor no es númerico");
		}
	}
	
	public static double calcularZ(double media, double desviacionEstandar, double alfa) {
		try {
			
			NormalDistribution distribuicao = new NormalDistribution(media, desviacionEstandar);
			double z = distribuicao.inverseCumulativeProbability(1-alfa);
			
			log.info("Calculo de Z: "+z);
			
			return z;
		} catch (Exception e) {
			return Double.NaN;
		}
	}
	
	public static double calcularTAlfaMedios(double gradosLibertad, double nivelC) {
		try {
			// Create T Distribution with N-1 degrees of freedom
			TDistribution tDist = new TDistribution(gradosLibertad);
			// Calculate critical value
			double critVal = tDist.inverseCumulativeProbability(1.0 - (1 - nivelC) / 2);
			// Calculate confidence interval
			return critVal;
		} catch (MathIllegalArgumentException e) {
			return Double.NaN;
		}
	}
	
	public static double calcularFAlfaMedios( double nivelC , double dfn , double dfd )
			throws IllegalArgumentException {
			double result = 0.0D;

			if ( ( dfn > 0.0D ) && ( dfd > 0.0D ) )
			{
				FDistribution fDist = new FDistribution(dfn, dfd);
				result = fDist.inverseCumulativeProbability(1.0 - (1 - nivelC) / 2);
				return result;
			}
			else
			{
				throw new IllegalArgumentException( "dfn or dfd <= 0" );
			}
		}
	
	public static double calcularChiAlfaMedios(double gradosLibertad, double nivelC) {
		try {
			// Create Chi Square Distribution with N-1 degrees of freedom
			ChiSquaredDistribution chiDist = new ChiSquaredDistribution(gradosLibertad);
			// Calculate critical value
			double critVal = chiDist.inverseCumulativeProbability(1.0 - (1 - nivelC) / 2);
			// Calculate confidence interval
			return critVal;
		} catch (MathIllegalArgumentException e) {
			return Double.NaN;
		}
	}
}
