package co.edu.usbcali.estadistica.vista;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;
import org.primefaces.component.panel.Panel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import co.edu.usbcali.estadistica.logica.Estimador;
import co.edu.usbcali.estadistica.logica.MuestreoAleatorioSimpleLogica;
import co.edu.usbcali.estadistica.logica.MuestreoAleatorioSistematicoLogica;
import co.edu.usbcali.estadistica.utils.FacesUtils;
import co.edu.usbcali.estadistica.utils.Utilidades;

@ManagedBean
@ViewScoped
public class MuestreoAleatorioSistematicoVista {
	
	private final static Logger log=LoggerFactory.getLogger(MuestreoAleatorioSistematicoVista.class);
	
	@Autowired
	private MuestreoAleatorioSistematicoLogica masLogica;
	
	private InputText txtTotalDatos;
	private InputNumber numTamanoMuestra;
	
	private CommandButton btnLimpiar;
	private CommandButton btnCalcular;
	
	private InputTextarea txtAreaDatos;
	
	private InputTextarea txtAreaMuestras;
	
	public String calcularAction() {
		log.info("Calcular Action");
		try {
			masLogica = new MuestreoAleatorioSistematicoLogica();
			log.info(txtAreaDatos.getValue().toString());
			masLogica.setDatos(Utilidades.obtenerDatosString(txtAreaDatos.getValue().toString()));
			txtTotalDatos.setValue(masLogica.getTotalDatos());
			if(numTamanoMuestra.getValue()==null) {
				throw new Exception("ingrese todos los datos");
			}
			masLogica.setTamanoMuestra(Utilidades.convertirStringToInt(numTamanoMuestra.getValue().toString().trim()));
			
			txtTotalDatos.setValue(masLogica.getTotalDatos());
			log.info(masLogica.hallarMuestras());
			txtAreaMuestras.setValue(masLogica.hallarMuestras());
			
			btnLimpiar.setDisabled(false);
		} catch (Exception e) {
			log.info("Error: "+e.getMessage());
			FacesUtils.addErrorMessage(e.getMessage());
		}
		return "";
	}
	
	public String crearInputsTamanoMuestras() throws Exception {
		try {
			
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		return "";
	}
	
	public String limpiarAction() {
		txtTotalDatos.resetValue();
		
		btnLimpiar.setDisabled(true);
		
		txtAreaDatos.resetValue();
		
		return "";
	}
	
	/**
	 * Inicio Getters and Setters
	 */
	public InputText getTxtTotalDatos() {
		return txtTotalDatos;
	}

	public void setTxtTotalDatos(InputText txtTotalDatos) {
		this.txtTotalDatos = txtTotalDatos;
	}

	public CommandButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(CommandButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}

	public CommandButton getBtnCalcular() {
		return btnCalcular;
	}

	public void setBtnCalcular(CommandButton btnCalcular) {
		this.btnCalcular = btnCalcular;
	}

	public InputTextarea getTxtAreaDatos() {
		return txtAreaDatos;
	}

	public void setTxtAreaDatos(InputTextarea txtAreaDatos) {
		this.txtAreaDatos = txtAreaDatos;
	}

	public InputNumber getNumTamanoMuestra() {
		return numTamanoMuestra;
	}

	public void setNumTamanoMuestra(InputNumber numTamanoMuestra) {
		this.numTamanoMuestra = numTamanoMuestra;
	}

	public InputTextarea getTxtAreaMuestras() {
		return txtAreaMuestras;
	}

	public void setTxtAreaMuestras(InputTextarea txtAreaMuestras) {
		this.txtAreaMuestras = txtAreaMuestras;
	}
	/**
	 * Fin Getters and Setters
	 */
}
