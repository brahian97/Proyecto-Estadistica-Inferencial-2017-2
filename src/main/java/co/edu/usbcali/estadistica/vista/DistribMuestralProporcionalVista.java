package co.edu.usbcali.estadistica.vista;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import co.edu.usbcali.estadistica.logica.DistribMuestralProporcional;
import co.edu.usbcali.estadistica.utils.FacesUtils;
import co.edu.usbcali.estadistica.utils.Utilidades;

@ManagedBean
@ViewScoped
public class DistribMuestralProporcionalVista {
private final static Logger log=LoggerFactory.getLogger(DistribMuestralProporcionalVista.class);
	
	@Autowired
	private DistribMuestralProporcional distribucionMP;
	
	private InputNumber txtProporcion;
	private InputNumber txtDato;
	private InputNumber txtMuestra;
	private SelectOneMenu somMenorMayor;
	private InputNumber txtDistribucion;
	
	private CommandButton btnLimpiar;
	private CommandButton btnCalcular;
		
	public String calcularAction() {
		log.info("Calcular Action");
		try {
			log.info("Dato 1: "+somMenorMayor.getValue()+".");
			distribucionMP = new DistribMuestralProporcional();
			
			distribucionMP.setMenorMayor(somMenorMayor.getValue().toString().trim());
			log.info("--------");
			distribucionMP.setP(Utilidades.convertirtxtValorDouble(txtProporcion.getValue().toString().trim()));
			distribucionMP.setDato(Utilidades.convertirtxtValorDouble(txtDato.getValue().toString().trim()));
			distribucionMP.setMuestra(Utilidades.convertirtxtValorDouble(txtMuestra.getValue().toString().trim()));
			
			txtDistribucion.setValue(""+distribucionMP.calcularDMP());
			btnLimpiar.setDisabled(false);
			
		} catch (Exception e) {
			log.info("Error: "+e.getMessage());
			FacesUtils.addErrorMessage(e.getMessage());
		}
		return "";
	}
	
	public String limpiarAction() {
		txtProporcion.resetValue();
		txtDato.resetValue();
		txtMuestra.resetValue();
		txtDistribucion.resetValue();
		somMenorMayor.resetValue();
		
		btnLimpiar.setDisabled(true);
			
		return "";
	}

	public SelectOneMenu getSomMenorMayor() {
		return somMenorMayor;
	}

	public void setSomMenorMayor(SelectOneMenu somMenorMayor) {
		this.somMenorMayor = somMenorMayor;
	}

	public CommandButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(CommandButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}

	public CommandButton getBtnCalcular() {
		return btnCalcular;
	}

	public void setBtnCalcular(CommandButton btnCalcular) {
		this.btnCalcular = btnCalcular;
	}

	public DistribMuestralProporcional getDistribucionMP() {
		return distribucionMP;
	}

	public void setDistribucionMP(DistribMuestralProporcional distribucionMP) {
		this.distribucionMP = distribucionMP;
	}

	public InputNumber getTxtProporcion() {
		return txtProporcion;
	}

	public void setTxtProporcion(InputNumber txtProporcion) {
		this.txtProporcion = txtProporcion;
	}

	public InputNumber getTxtDato() {
		return txtDato;
	}

	public void setTxtDato(InputNumber txtDato) {
		this.txtDato = txtDato;
	}

	public InputNumber getTxtMuestra() {
		return txtMuestra;
	}

	public void setTxtMuestra(InputNumber txtMuestra) {
		this.txtMuestra = txtMuestra;
	}

	public InputNumber getTxtDistribucion() {
		return txtDistribucion;
	}

	public void setTxtDistribucion(InputNumber txtDistribucion) {
		this.txtDistribucion = txtDistribucion;
	}

	
	
	
}
