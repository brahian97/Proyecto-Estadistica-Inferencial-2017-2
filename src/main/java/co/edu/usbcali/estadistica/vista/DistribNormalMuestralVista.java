package co.edu.usbcali.estadistica.vista;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import co.edu.usbcali.estadistica.logica.DistribNormalMuestral;
import co.edu.usbcali.estadistica.utils.FacesUtils;
import co.edu.usbcali.estadistica.utils.Statistics;
import co.edu.usbcali.estadistica.utils.Utilidades;

@ManagedBean
@ViewScoped
public class DistribNormalMuestralVista {
private final static Logger log=LoggerFactory.getLogger(DistribNormalMuestralVista.class);
	
	@Autowired
	private DistribNormalMuestral distribucionNM;
	
	Statistics stat;
	
	private InputNumber txtMedia;
	private InputNumber txtDesviacion;
	private InputNumber txtZ;
	private InputNumber txtZMenor;
	private InputNumber txtZMayor;
	private InputNumber txtMuestra;
	private InputNumber txtResultado;
	private SelectOneMenu somMenorMayor;
	
	
	private CommandButton btnLimpiar;
	private CommandButton btnCalcular;
		
	public String calcularAction() {
		log.info("Calcular Action");

		try {
			
			if(txtMuestra.getValue()==null) {
				throw new Exception("La muestra no puede estar vacía, si no hay muestra ingrese un '0'");
			}
				log.info("Dato 1: "+txtMedia.getValue());
				
				distribucionNM = new DistribNormalMuestral();
				distribucionNM.setDato(Utilidades.convertirtxtValorDouble(txtZ.getValue().toString().trim()));
				distribucionNM.setMuestra(Utilidades.convertirtxtValorDouble(txtMuestra.getValue().toString().trim()));
				distribucionNM.setDe(Utilidades.convertirtxtValorDouble(txtDesviacion.getValue().toString().trim()));
				distribucionNM.setMedia(Utilidades.convertirtxtValorDouble(txtMedia.getValue().toString().trim()));
				distribucionNM.setMenorMayor(somMenorMayor.getValue().toString().trim());
				txtResultado.setValue(""+distribucionNM.calcularDNM());
				
				btnLimpiar.setDisabled(false);
			
			
		} catch (Exception e) {
			log.info("Error: "+e.getMessage());
			FacesUtils.addErrorMessage(e.getMessage());
		}
		return "";
		
	}
	
	public String calcularEntreAction() {
		log.info("CalcularEntreAction");

		try {
			
				log.info("Dato 1: "+txtMedia.getValue());
				
				distribucionNM = new DistribNormalMuestral();
				distribucionNM.setDatoMenor(Utilidades.convertirtxtValorDouble(txtZMenor.getValue().toString()));
				distribucionNM.setDatoMayor(Utilidades.convertirtxtValorDouble(txtZMayor.getValue().toString()));
				distribucionNM.setMuestra(Utilidades.convertirtxtValorDouble(txtMuestra.getValue().toString().trim()));
				distribucionNM.setDe(Utilidades.convertirtxtValorDouble(txtDesviacion.getValue().toString().trim()));
				distribucionNM.setMedia(Utilidades.convertirtxtValorDouble(txtMedia.getValue().toString().trim()));
				txtResultado.setValue(""+distribucionNM.calcularDNMEntre());
				
				
				btnLimpiar.setDisabled(false);
			
			
		} catch (Exception e) {
			log.info("Error: "+e.getMessage());
			FacesUtils.addErrorMessage(e.getMessage());
		}
		return "";
		
	}

	
	public String limpiarAction() {
		txtMedia.resetValue();
		txtDesviacion.resetValue();
		txtZ.resetValue();
		txtMuestra.resetValue();
		txtResultado.resetValue();
		somMenorMayor.resetValue();
		
		btnLimpiar.setDisabled(false);
			
		return "";
	}

	
	public SelectOneMenu getSomMenorMayor() {
		return somMenorMayor;
	}

	public void setSomMenorMayor(SelectOneMenu somMenorMayor) {
		this.somMenorMayor = somMenorMayor;
	}

	public CommandButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(CommandButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}

	public CommandButton getBtnCalcular() {
		return btnCalcular;
	}

	public void setBtnCalcular(CommandButton btnCalcular) {
		this.btnCalcular = btnCalcular;
	}


	public InputNumber getTxtMedia() {
		return txtMedia;
	}


	public void setTxtMedia(InputNumber txtMedia) {
		this.txtMedia = txtMedia;
	}


	public InputNumber getTxtDesviacion() {
		return txtDesviacion;
	}


	public void setTxtDesviacion(InputNumber txtDesviacion) {
		this.txtDesviacion = txtDesviacion;
	}


	public InputNumber getTxtZ() {
		return txtZ;
	}


	public void setTxtZ(InputNumber txtZ) {
		this.txtZ = txtZ;
	}


	public InputNumber getTxtMuestra() {
		return txtMuestra;
	}


	public void setTxtMuestra(InputNumber txtMuestra) {
		this.txtMuestra = txtMuestra;
	}


	public InputNumber getTxtResultado() {
		return txtResultado;
	}


	public void setTxtResultado(InputNumber txtResultado) {
		this.txtResultado = txtResultado;
	}

	public InputNumber getTxtZMenor() {
		return txtZMenor;
	}

	public void setTxtZMenor(InputNumber txtZMenor) {
		this.txtZMenor = txtZMenor;
	}

	public InputNumber getTxtZMayor() {
		return txtZMayor;
	}

	public void setTxtZMayor(InputNumber txtZMayor) {
		this.txtZMayor = txtZMayor;
	}
	
	
	
	
}
