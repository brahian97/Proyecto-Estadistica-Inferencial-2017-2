package co.edu.usbcali.estadistica.vista;

import java.text.DecimalFormat;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.panelgrid.PanelGrid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import co.edu.usbcali.estadistica.logica.IntervaloConfianza;
import co.edu.usbcali.estadistica.utils.FacesUtils;
import co.edu.usbcali.estadistica.utils.Utilidades;

@ManagedBean
@ViewScoped
public class IntervaloConfianzaVista {
private final static Logger log=LoggerFactory.getLogger(IntervaloConfianzaVista.class);
	
	@Autowired
	private IntervaloConfianza intervaloConfianza;
	
	private InputNumber numDesviacionEstandar;
	private InputNumber numMuestra;
	private InputNumber numMediaMuestral;
	private InputNumber numNivelConfianza;
	private InputNumber numAlfa;
	private OutputLabel oplZUno;
	private OutputLabel oplICUno;
	private OutputLabel oplInterpretacionUno;
	private OutputLabel oplTDos;
	private OutputLabel oplVDos;
	private OutputLabel oplICDos;
	private OutputLabel oplInterpretacionDos;
	private PanelGrid pnlCasoUno;
	private PanelGrid pnlCasoDos;
	private OutputLabel oplCaso;
	private OutputLabel oplCaso1;
	
	private CommandButton btnLimpiar;
	private CommandButton btnCalcular;
	
	public double[] resultadoIntervalo;
		
	public String calcularAction() {
		log.info("Calcular Action");
		try {
			if(numAlfa.getValue()==null && numDesviacionEstandar.getValue()==null){
				throw new Exception("Debes ingresar desviación estándar, ya sea poblacional o muestral");
			}
			if(numMuestra.getValue()==null || numMediaMuestral.getValue()==null){
				throw new Exception("Falta alguno de los datos necesarios para realizar el cálculo");
			}
			
			intervaloConfianza = new IntervaloConfianza();
			intervaloConfianza.setNivelConfianza(Utilidades.convertirtxtValorDouble(numNivelConfianza.getValue().toString()));
			intervaloConfianza.setMuestra(Utilidades.convertirtxtValorDouble(numMuestra.getValue().toString()));
			log.info("Num----"+numDesviacionEstandar.getValue());
			if(numDesviacionEstandar.getValue()==null){
				if(Utilidades.convertirtxtValorDouble(numMuestra.getValue().toString())>=30D){
					intervaloConfianza.setVarianzaPob(Utilidades.convertirtxtValorDouble(numAlfa.getValue().toString()));
				}else{
					intervaloConfianza.setVarianzaPob(0.0);
					intervaloConfianza.setVarianzaMue(Utilidades.convertirtxtValorDouble(numAlfa.getValue().toString()));
				}
				
			}else{
				intervaloConfianza.setVarianzaPob(Utilidades.convertirtxtValorDouble(numDesviacionEstandar.getValue().toString())
						*Utilidades.convertirtxtValorDouble(numDesviacionEstandar.getValue().toString()));
			}
			
			intervaloConfianza.setMediaMuestral(Utilidades.convertirtxtValorDouble(numMediaMuestral.getValue().toString()));
			
			intervaloConfianza.CalcularIntervaloMedia();
			
			if(intervaloConfianza.getVarianzaPob()!=0D && intervaloConfianza.getMuestra()>=30){
				
				DecimalFormat df = new DecimalFormat("#.00");
				
				oplZUno.setValue(intervaloConfianza.getzAlfaMedios());
				oplICUno.setValue(df.format(intervaloConfianza.getLimInferior())+"; "+df.format(intervaloConfianza.getLimSuperior()));
				oplInterpretacionUno.setValue("con un nivel de confianza del "
				+Utilidades.convertirtxtValorDouble(numNivelConfianza.getValue().toString())*100+
				"% se puede decir que uno de los intervalos que contiene la media poblacional se encuentra entre "
				+df.format(intervaloConfianza.getLimInferior())+" Y "+df.format(intervaloConfianza.getLimSuperior()));
				
				pnlCasoUno.setStyle("display: grid;");
				
				oplCaso1.setValue("1 (varianza conocida o n mayor a treinta)");
				
			}else{
				DecimalFormat df = new DecimalFormat("#.00");
				
				oplTDos.setValue(df.format(intervaloConfianza.getCritVal()));
				oplVDos.setValue(df.format(intervaloConfianza.getMuestra()-1));
				oplICDos.setValue(df.format(intervaloConfianza.getLimInferior())+"; "+df.format(intervaloConfianza.getLimSuperior()));
				oplInterpretacionDos.setValue("con un nivel de confianza del "
				+df.format(Utilidades.convertirtxtValorDouble(numNivelConfianza.getValue().toString())*100)+
				"% se puede decir que uno de los intervalos que contiene la media poblacional se encuentra entre "
				+df.format(intervaloConfianza.getLimInferior())+" Y "+df.format(intervaloConfianza.getLimSuperior()));
				
				pnlCasoDos.setStyle("display: grid;");
				
				oplCaso.setValue("2 (varianza desconocida y n menor a treinta)");
			}
			btnLimpiar.setDisabled(false);
			
		} catch (Exception e) {
			FacesUtils.addErrorMessage(e.getMessage());
		}
		return "";
	}
	
	public String limpiarAction() {
		numDesviacionEstandar.resetValue();
		numMuestra.resetValue();
		numMediaMuestral.resetValue();
		numAlfa.resetValue();
		oplZUno.resetValue();
		oplICUno.resetValue();
		oplInterpretacionUno.resetValue();
		oplTDos.resetValue();
		oplVDos.resetValue();
		oplICDos.resetValue();
		oplInterpretacionDos.resetValue();
		pnlCasoUno.setStyle("display: none");
		pnlCasoDos.setStyle("display: none");;
		
		btnLimpiar.setDisabled(true);
			
		return "";
	}

	public InputNumber getNumDesviacionEstandar() {
		return numDesviacionEstandar;
	}

	public void setNumDesviacionEstandar(InputNumber numDesviacionEstandar) {
		this.numDesviacionEstandar = numDesviacionEstandar;
	}

	public InputNumber getNumMuestra() {
		return numMuestra;
	}

	public void setNumMuestra(InputNumber numMuestra) {
		this.numMuestra = numMuestra;
	}

	public InputNumber getNumMediaMuestral() {
		return numMediaMuestral;
	}

	public void setNumMediaMuestral(InputNumber numMediaMuestral) {
		this.numMediaMuestral = numMediaMuestral;
	}

	public CommandButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(CommandButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}

	public CommandButton getBtnCalcular() {
		return btnCalcular;
	}

	public void setBtnCalcular(CommandButton btnCalcular) {
		this.btnCalcular = btnCalcular;
	}

	public InputNumber getNumNivelConfianza() {
		return numNivelConfianza;
	}

	public void setNumNivelConfianza(InputNumber numNivelConfianza) {
		this.numNivelConfianza = numNivelConfianza;
	}

	public InputNumber getNumAlfa() {
		return numAlfa;
	}

	public void setNumAlfa(InputNumber numAlfa) {
		this.numAlfa = numAlfa;
	}

	public OutputLabel getOplZUno() {
		return oplZUno;
	}

	public void setOplZUno(OutputLabel oplZUno) {
		this.oplZUno = oplZUno;
	}

	public OutputLabel getOplICUno() {
		return oplICUno;
	}

	public void setOplICUno(OutputLabel oplICUno) {
		this.oplICUno = oplICUno;
	}

	public OutputLabel getOplInterpretacionUno() {
		return oplInterpretacionUno;
	}

	public void setOplInterpretacionUno(OutputLabel oplInterpretacionUno) {
		this.oplInterpretacionUno = oplInterpretacionUno;
	}

	public OutputLabel getOplTDos() {
		return oplTDos;
	}

	public void setOplTDos(OutputLabel oplTDos) {
		this.oplTDos = oplTDos;
	}

	public OutputLabel getOplVDos() {
		return oplVDos;
	}

	public void setOplVDos(OutputLabel oplVDos) {
		this.oplVDos = oplVDos;
	}

	public OutputLabel getOplICDos() {
		return oplICDos;
	}

	public void setOplICDos(OutputLabel oplICDos) {
		this.oplICDos = oplICDos;
	}

	public OutputLabel getOplInterpretacionDos() {
		return oplInterpretacionDos;
	}

	public void setOplInterpretacionDos(OutputLabel oplInterpretacionDos) {
		this.oplInterpretacionDos = oplInterpretacionDos;
	}

	public double[] getResultadoIntervalo() {
		return resultadoIntervalo;
	}

	public void setResultadoIntervalo(double[] resultadoIntervalo) {
		this.resultadoIntervalo = resultadoIntervalo;
	}

	public PanelGrid getPnlCasoUno() {
		return pnlCasoUno;
	}

	public void setPnlCasoUno(PanelGrid pnlCasoUno) {
		this.pnlCasoUno = pnlCasoUno;
	}

	public PanelGrid getPnlCasoDos() {
		return pnlCasoDos;
	}

	public void setPnlCasoDos(PanelGrid pnlCasoDos) {
		this.pnlCasoDos = pnlCasoDos;
	}

	public OutputLabel getOplCaso() {
		return oplCaso;
	}

	public void setOplCaso(OutputLabel oplCaso) {
		this.oplCaso = oplCaso;
	}
	
	public OutputLabel getOplCaso1() {
		return oplCaso1;
	}

	public void setOplCaso1(OutputLabel oplCaso1) {
		this.oplCaso1 = oplCaso1;
	}

}
