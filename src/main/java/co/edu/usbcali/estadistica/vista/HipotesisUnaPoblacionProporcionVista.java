package co.edu.usbcali.estadistica.vista;

import java.text.DecimalFormat;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import co.edu.usbcali.estadistica.logica.PruebaHipotesis;
import co.edu.usbcali.estadistica.utils.FacesUtils;
import co.edu.usbcali.estadistica.utils.Utilidades;

@ManagedBean
@ViewScoped
public class HipotesisUnaPoblacionProporcionVista {

	private final static Logger log = LoggerFactory.getLogger(HipotesisUnaPoblacionProporcionVista.class);

	@Autowired
	private PruebaHipotesis pruebaHipotesis;

	private InputNumber numHipotesisNula;
	private InputNumber numHipotesisAlterna;
	private SelectOneMenu somSimbolo;
	private InputNumber numNivelSignificancia;
	private InputNumber numDesviacionEstandar;
	private InputNumber numTamano;

	private InputNumber numProporcion;

	private OutputLabel oplZTeorico;
	private OutputLabel oplZCalculado;
	private OutputLabel oplInterpretacion;

	private CommandButton btnLimpiar;
	private CommandButton btnCalcular;

	public String calcularAction() {
		log.info("Calcular Action");
		try {

			if (numHipotesisAlterna == null || numHipotesisNula == null || numNivelSignificancia == null
					|| numTamano == null || numProporcion == null) {
				throw new Exception("Ingrese todos los campos");
			}

			pruebaHipotesis = new PruebaHipotesis();

			pruebaHipotesis
					.setHipotesisNula(Utilidades.convertirtxtValorDouble(numHipotesisNula.getValue().toString()));
			pruebaHipotesis
					.setHipotesisAlterna(Utilidades.convertirtxtValorDouble(numHipotesisAlterna.getValue().toString()));
			pruebaHipotesis.setProporcion(Utilidades.convertirtxtValorDouble(numProporcion.getValue().toString()));
			pruebaHipotesis.setNivelConfiabilidad(
					Utilidades.convertirtxtValorDouble(numNivelSignificancia.getValue().toString()));
			pruebaHipotesis.setSimbolo(somSimbolo.getValue().toString());
			pruebaHipotesis.setTamano(Utilidades.convertirStringToInt(numTamano.getValue().toString()));

			pruebaHipotesis.calcularHipotesisProporcion();

			DecimalFormat df = new DecimalFormat("#.00");

			if (pruebaHipotesis.isUseZ()) {
				oplZTeorico.setValue(df.format(pruebaHipotesis.getzTeorico()));
				oplZCalculado.setValue(df.format(pruebaHipotesis.getzCalculado()));
				oplInterpretacion.setValue(pruebaHipotesis.getInterpretacion());
			} else {
				oplZTeorico.setValue(df.format(pruebaHipotesis.gettTeorico()));
				oplZCalculado.setValue(df.format(pruebaHipotesis.gettCalculado()));
				oplInterpretacion.setValue(pruebaHipotesis.getInterpretacion());
			}

		} catch (Exception e) {
			FacesUtils.addErrorMessage(e.getMessage());
		}
		return "";
	}

	public String limpiarAction() {

		return "";
	}

	public PruebaHipotesis getPruebaHipotesis() {
		return pruebaHipotesis;
	}

	public void setPruebaHipotesis(PruebaHipotesis pruebaHipotesis) {
		this.pruebaHipotesis = pruebaHipotesis;
	}

	public InputNumber getNumHipotesisNula() {
		return numHipotesisNula;
	}

	public void setNumHipotesisNula(InputNumber numHipotesisNula) {
		this.numHipotesisNula = numHipotesisNula;
	}

	public InputNumber getNumHipotesisAlterna() {
		return numHipotesisAlterna;
	}

	public void setNumHipotesisAlterna(InputNumber numHipotesisAlterna) {
		this.numHipotesisAlterna = numHipotesisAlterna;
	}

	public SelectOneMenu getSomSimbolo() {
		return somSimbolo;
	}

	public void setSomSimbolo(SelectOneMenu somSimbolo) {
		this.somSimbolo = somSimbolo;
	}

	public InputNumber getNumNivelSignificancia() {
		return numNivelSignificancia;
	}

	public void setNumNivelSignificancia(InputNumber numNivelSignificancia) {
		this.numNivelSignificancia = numNivelSignificancia;
	}

	public InputNumber getNumDesviacionEstandar() {
		return numDesviacionEstandar;
	}

	public void setNumDesviacionEstandar(InputNumber numDesviacionEstandar) {
		this.numDesviacionEstandar = numDesviacionEstandar;
	}

	public InputNumber getNumTamano() {
		return numTamano;
	}

	public void setNumTamano(InputNumber numTamano) {
		this.numTamano = numTamano;
	}

	public InputNumber getNumProporcion() {
		return numProporcion;
	}

	public void setNumProporcion(InputNumber numProporcion) {
		this.numProporcion = numProporcion;
	}

	public CommandButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(CommandButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}

	public CommandButton getBtnCalcular() {
		return btnCalcular;
	}

	public void setBtnCalcular(CommandButton btnCalcular) {
		this.btnCalcular = btnCalcular;
	}

	public OutputLabel getOplZTeorico() {
		return oplZTeorico;
	}

	public void setOplZTeorico(OutputLabel oplZTeorico) {
		this.oplZTeorico = oplZTeorico;
	}

	public OutputLabel getOplZCalculado() {
		return oplZCalculado;
	}

	public void setOplZCalculado(OutputLabel oplZCalculado) {
		this.oplZCalculado = oplZCalculado;
	}

	public OutputLabel getOplInterpretacion() {
		return oplInterpretacion;
	}

	public void setOplInterpretacion(OutputLabel oplInterpretacion) {
		this.oplInterpretacion = oplInterpretacion;
	}
}
