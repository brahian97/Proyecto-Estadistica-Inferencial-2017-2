package co.edu.usbcali.estadistica.vista;

import java.text.DecimalFormat;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import co.edu.usbcali.estadistica.logica.Parametro;
import co.edu.usbcali.estadistica.utils.FacesUtils;
import co.edu.usbcali.estadistica.utils.Utilidades;

@ManagedBean
@ViewScoped
public class ParametrosVista {
	
	private final static Logger log=LoggerFactory.getLogger(ParametrosVista.class);
	
	@Autowired
	private Parametro parametro;
	
	private InputText txtMediaPoblacional;
	private InputText txtModaPoblacional;
	private InputText txtMedianaPoblacional;
	private InputText txtTotalDatos;
	private InputText txtVarianzaPoblacional;
	private InputText txtDesviacionEstandarPoblacional;
	
	private CommandButton btnLimpiar;
	private CommandButton btnCalcular;
	
	private InputTextarea txtAreaDatos;
	
	public String calcularAction() {
		log.info("Calcular Action");
		try {
			parametro = new Parametro();
			DecimalFormat df = new DecimalFormat("#.00");
			log.info(txtAreaDatos.getValue().toString());
			parametro.setDatos(Utilidades.obtenerDatosDouble(txtAreaDatos.getValue().toString()));
			txtTotalDatos.setValue(parametro.getTotalDatos());
			txtMediaPoblacional.setValue(parametro.getMedia());
			txtVarianzaPoblacional.setValue(parametro.getVarianza());
			txtDesviacionEstandarPoblacional.setValue(df.format(parametro.getDesviacionEstandar()));
			txtModaPoblacional.setValue(parametro.getModa());
			String mediana="";
			for(int i=0; i<parametro.getMediana().length; i++) {
				mediana = String.valueOf(parametro.getMediana()[i]);
				mediana.concat(" ,");
			}
			txtMedianaPoblacional.setValue(mediana.substring(0, mediana.length()-1));
			btnLimpiar.setDisabled(false);
		} catch (Exception e) {
			log.info("Error: "+e.getMessage());
			FacesUtils.addErrorMessage(e.getMessage());
		}
		return "";
	}
	
	public String limpiarAction() {
		txtMediaPoblacional.resetValue();
		txtModaPoblacional.resetValue();
		txtMedianaPoblacional.resetValue();
		txtTotalDatos.resetValue();
		txtVarianzaPoblacional.resetValue();
		txtDesviacionEstandarPoblacional.resetValue();
		
		btnLimpiar.setDisabled(true);
		
		txtAreaDatos.resetValue();
		
		return "";
	}
	
	/**
	 * Inicio Getters and Setters
	 */
	public InputText getTxtMediaPoblacional() {
		return txtMediaPoblacional;
	}

	public void setTxtMediaPoblacional(InputText txtMediaPoblacional) {
		this.txtMediaPoblacional = txtMediaPoblacional;
	}

	public InputText getTxtModaPoblacional() {
		return txtModaPoblacional;
	}

	public void setTxtModaPoblacional(InputText txtModaPoblacional) {
		this.txtModaPoblacional = txtModaPoblacional;
	}

	public InputText getTxtMedianaPoblacional() {
		return txtMedianaPoblacional;
	}

	public void setTxtMedianaPoblacional(InputText txtMedianaPoblacional) {
		this.txtMedianaPoblacional = txtMedianaPoblacional;
	}

	public InputText getTxtTotalDatos() {
		return txtTotalDatos;
	}

	public void setTxtTotalDatos(InputText txtTotalDatos) {
		this.txtTotalDatos = txtTotalDatos;
	}

	public InputText getTxtVarianzaPoblacional() {
		return txtVarianzaPoblacional;
	}

	public void setTxtVarianzaPoblacional(InputText txtVarianzaPoblacional) {
		this.txtVarianzaPoblacional = txtVarianzaPoblacional;
	}

	public InputText getTxtDesviacionEstandarPoblacional() {
		return txtDesviacionEstandarPoblacional;
	}

	public void setTxtDesviacionEstandarPoblacional(InputText txtDesviacionEstandarPoblacional) {
		this.txtDesviacionEstandarPoblacional = txtDesviacionEstandarPoblacional;
	}

	public CommandButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(CommandButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}

	public CommandButton getBtnCalcular() {
		return btnCalcular;
	}

	public void setBtnCalcular(CommandButton btnCalcular) {
		this.btnCalcular = btnCalcular;
	}

	public InputTextarea getTxtAreaDatos() {
		return txtAreaDatos;
	}

	public void setTxtAreaDatos(InputTextarea txtAreaDatos) {
		this.txtAreaDatos = txtAreaDatos;
	}
	/**
	 * Fin Getters and Setters
	 */
}
