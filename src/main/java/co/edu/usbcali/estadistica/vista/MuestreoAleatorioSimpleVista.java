package co.edu.usbcali.estadistica.vista;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;
import org.primefaces.component.panel.Panel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import co.edu.usbcali.estadistica.logica.Estimador;
import co.edu.usbcali.estadistica.logica.MuestreoAleatorioSimpleLogica;
import co.edu.usbcali.estadistica.utils.FacesUtils;
import co.edu.usbcali.estadistica.utils.Utilidades;

@ManagedBean
@ViewScoped
public class MuestreoAleatorioSimpleVista {
	
	private final static Logger log=LoggerFactory.getLogger(MuestreoAleatorioSimpleVista.class);
	
	@Autowired
	private MuestreoAleatorioSimpleLogica masLogica;
	
	private InputText txtTotalDatos;
	private InputNumber numNumeroMuestras;
	private InputNumber numTamanoMuestras;
	private InputTextarea txtAreaMuestras;
	
	private CommandButton btnLimpiar;
	private CommandButton btnCalcular;
	
	private InputTextarea txtAreaDatos;
	
	public String calcularAction() {
		log.info("Calcular Action");
		try {
			masLogica = new MuestreoAleatorioSimpleLogica();
			
			masLogica.setDatos(Utilidades.obtenerDatosString(txtAreaDatos.getValue().toString()));
			if(numNumeroMuestras.getValue()==null || numTamanoMuestras.getValue()==null) {
				throw new Exception("ingrese todos los datos");
			}
			masLogica.setNumeroMuestra(Utilidades.convertirStringToInt(numNumeroMuestras.getValue().toString().trim()));
			masLogica.setTamanoMuestra(Utilidades.convertirStringToInt(numTamanoMuestras.getValue().toString().trim()));
			
			txtTotalDatos.setValue(masLogica.getTotalDatos());
			log.info(masLogica.hallarMuestras());
			txtAreaMuestras.setValue(masLogica.hallarMuestras());
			
			btnLimpiar.setDisabled(false);
		} catch (Exception e) {
			FacesUtils.addErrorMessage(e.getMessage());
		}
		return "";
	}
	
	public String crearInputsTamanoMuestras() throws Exception {
		try {
			
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		return "";
	}
	
	public String limpiarAction() {
		txtTotalDatos.resetValue();
		numNumeroMuestras.resetValue();
		numTamanoMuestras.resetValue();
		txtAreaMuestras.resetValue();
		
		btnLimpiar.setDisabled(true);
		
		txtAreaDatos.resetValue();
		
		return "";
	}
	
	/**
	 * Inicio Getters and Setters
	 */
	public InputText getTxtTotalDatos() {
		return txtTotalDatos;
	}

	public void setTxtTotalDatos(InputText txtTotalDatos) {
		this.txtTotalDatos = txtTotalDatos;
	}

	public CommandButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(CommandButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}

	public CommandButton getBtnCalcular() {
		return btnCalcular;
	}

	public void setBtnCalcular(CommandButton btnCalcular) {
		this.btnCalcular = btnCalcular;
	}

	public InputTextarea getTxtAreaDatos() {
		return txtAreaDatos;
	}

	public void setTxtAreaDatos(InputTextarea txtAreaDatos) {
		this.txtAreaDatos = txtAreaDatos;
	}

	public InputNumber getNumNumeroMuestras() {
		return numNumeroMuestras;
	}

	public void setNumNumeroMuestras(InputNumber numNumeroMuestras) {
		this.numNumeroMuestras = numNumeroMuestras;
	}

	public InputNumber getNumTamanoMuestras() {
		return numTamanoMuestras;
	}

	public void setNumTamanoMuestras(InputNumber numTamanoMuestras) {
		this.numTamanoMuestras = numTamanoMuestras;
	}

	public InputTextarea getTxtAreaMuestras() {
		return txtAreaMuestras;
	}

	public void setTxtAreaMuestras(InputTextarea txtAreaMuestras) {
		this.txtAreaMuestras = txtAreaMuestras;
	}
	
	
	/**
	 * Fin Getters and Setters
	 */
}
