package co.edu.usbcali.estadistica.vista;

import java.text.DecimalFormat;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.panelgrid.PanelGrid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import co.edu.usbcali.estadistica.logica.IntervaloConfianza;
import co.edu.usbcali.estadistica.utils.FacesUtils;
import co.edu.usbcali.estadistica.utils.Utilidades;

@ManagedBean
@ViewScoped
public class IntervaloConfianzaPropVista {
private final static Logger log=LoggerFactory.getLogger(IntervaloConfianzaPropVista.class);
	
	@Autowired
	private IntervaloConfianza intervaloConfianza;
	
	private InputNumber numPGorro;
	private InputNumber numMuestra;
	private InputNumber numQGorro;
	private InputNumber numNivelConfianza;
	private InputNumber numAlfa;
	private InputNumber numAlfaMedios;
	private OutputLabel oplZUno;
	private OutputLabel oplICUno;
	private OutputLabel oplInterpretacionUno;
	private OutputLabel oplTDos;
	private OutputLabel oplVDos;
	private OutputLabel oplICDos;
	private OutputLabel oplInterpretacionDos;
	private PanelGrid pnlCasoUno;
	private PanelGrid pnlCasoDos;
	
	private CommandButton btnLimpiar;
	private CommandButton btnCalcular;
	
	public double[] resultadoIntervalo;
		
	public String calcularAction() {
		log.info("Calcular Action");
		try {
			intervaloConfianza = new IntervaloConfianza();
			
			intervaloConfianza.setP(Utilidades.convertirtxtValorDouble(numPGorro.getValue().toString()));
			intervaloConfianza.setMuestra(Utilidades.convertirtxtValorDouble(numMuestra.getValue().toString()));
			intervaloConfianza.setNivelConfianza(Utilidades.convertirtxtValorDouble(numNivelConfianza.getValue().toString()));
			
			intervaloConfianza.calcularIntervaloProporcion();
			numQGorro.setValue(intervaloConfianza.getQ());
			numAlfa.setValue(1-intervaloConfianza.getNivelConfianza());
			numAlfaMedios.setValue((1-intervaloConfianza.getNivelConfianza())/2);
			if(numPGorro.getValue()!=null && numMuestra.getValue()!=null && numNivelConfianza.getValue()!=null){
				if(Utilidades.convertirtxtValorDouble(numMuestra.getValue().toString())>=30){
					
					DecimalFormat df = new DecimalFormat("#.00");
					
					oplZUno.setValue(intervaloConfianza.getzAlfaMedios());
					oplICUno.setValue(df.format(intervaloConfianza.getLimInferior())+" ; "+df.format(intervaloConfianza.getLimSuperior()));
					oplInterpretacionUno.setValue("con un nivel de confianza del "
					+Utilidades.convertirtxtValorDouble(numNivelConfianza.getValue().toString())*100+
					"% se puede decir que uno de los intervalos que contiene la proporción poblacional se encuentra entre "
					+df.format(intervaloConfianza.getLimInferior())+" Y "+df.format(intervaloConfianza.getLimSuperior()));
					
					pnlCasoUno.setStyle("display: grid;");
				}else{
					throw new Exception("El tamaño de la muestra debe ser como mínimo de 30");
				}
			}else{
				throw new Exception("Falta ingresar uno o varios datos obligatorios");
			}
			
			btnLimpiar.setDisabled(false);
			
		} catch (Exception e) {
			FacesUtils.addErrorMessage(e.getMessage());
		}
		return "";
	}
	
	public String limpiarAction() {
		numPGorro.resetValue();
		numMuestra.resetValue();
		numQGorro.resetValue();
		numAlfa.resetValue();
		numAlfaMedios.resetValue();
		oplZUno.resetValue();
		oplICUno.resetValue();
		oplInterpretacionUno.resetValue();
		oplTDos.resetValue();
		oplVDos.resetValue();
		oplICDos.resetValue();
		oplInterpretacionDos.resetValue();
		pnlCasoUno.setStyle("display: none");
		pnlCasoDos.setStyle("display: none");;
		
		btnLimpiar.setDisabled(true);
			
		return "";
	}

	
	public InputNumber getNumPGorro() {
		return numPGorro;
	}

	public void setNumPGorro(InputNumber numPGorro) {
		this.numPGorro = numPGorro;
	}

	public InputNumber getNumMuestra() {
		return numMuestra;
	}

	public void setNumMuestra(InputNumber numMuestra) {
		this.numMuestra = numMuestra;
	}

	public InputNumber getNumQGorro() {
		return numQGorro;
	}

	public void setNumQGorro(InputNumber numQGorro) {
		this.numQGorro = numQGorro;
	}

	public CommandButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(CommandButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}

	public CommandButton getBtnCalcular() {
		return btnCalcular;
	}

	public void setBtnCalcular(CommandButton btnCalcular) {
		this.btnCalcular = btnCalcular;
	}

	public InputNumber getNumNivelConfianza() {
		return numNivelConfianza;
	}

	public void setNumNivelConfianza(InputNumber numNivelConfianza) {
		this.numNivelConfianza = numNivelConfianza;
	}

	public InputNumber getNumAlfa() {
		return numAlfa;
	}

	public void setNumAlfa(InputNumber numAlfa) {
		this.numAlfa = numAlfa;
	}

	public OutputLabel getOplZUno() {
		return oplZUno;
	}

	public void setOplZUno(OutputLabel oplZUno) {
		this.oplZUno = oplZUno;
	}

	public OutputLabel getOplICUno() {
		return oplICUno;
	}

	public void setOplICUno(OutputLabel oplICUno) {
		this.oplICUno = oplICUno;
	}

	public OutputLabel getOplInterpretacionUno() {
		return oplInterpretacionUno;
	}

	public void setOplInterpretacionUno(OutputLabel oplInterpretacionUno) {
		this.oplInterpretacionUno = oplInterpretacionUno;
	}

	public OutputLabel getOplTDos() {
		return oplTDos;
	}

	public void setOplTDos(OutputLabel oplTDos) {
		this.oplTDos = oplTDos;
	}

	public OutputLabel getOplVDos() {
		return oplVDos;
	}

	public void setOplVDos(OutputLabel oplVDos) {
		this.oplVDos = oplVDos;
	}

	public OutputLabel getOplICDos() {
		return oplICDos;
	}

	public void setOplICDos(OutputLabel oplICDos) {
		this.oplICDos = oplICDos;
	}

	public OutputLabel getOplInterpretacionDos() {
		return oplInterpretacionDos;
	}

	public void setOplInterpretacionDos(OutputLabel oplInterpretacionDos) {
		this.oplInterpretacionDos = oplInterpretacionDos;
	}

	public double[] getResultadoIntervalo() {
		return resultadoIntervalo;
	}

	public void setResultadoIntervalo(double[] resultadoIntervalo) {
		this.resultadoIntervalo = resultadoIntervalo;
	}

	public PanelGrid getPnlCasoUno() {
		return pnlCasoUno;
	}

	public void setPnlCasoUno(PanelGrid pnlCasoUno) {
		this.pnlCasoUno = pnlCasoUno;
	}

	public PanelGrid getPnlCasoDos() {
		return pnlCasoDos;
	}

	public void setPnlCasoDos(PanelGrid pnlCasoDos) {
		this.pnlCasoDos = pnlCasoDos;
	}

	public InputNumber getNumAlfaMedios() {
		return numAlfaMedios;
	}

	public void setNumAlfaMedios(InputNumber numAlfaMedios) {
		this.numAlfaMedios = numAlfaMedios;
	}

}
