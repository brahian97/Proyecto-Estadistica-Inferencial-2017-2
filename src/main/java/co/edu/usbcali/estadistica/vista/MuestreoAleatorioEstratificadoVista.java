package co.edu.usbcali.estadistica.vista;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;
import org.primefaces.component.panel.Panel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import co.edu.usbcali.estadistica.logica.Estimador;
import co.edu.usbcali.estadistica.logica.MuestreoAleatorioSimpleLogica;
import co.edu.usbcali.estadistica.utils.FacesUtils;
import co.edu.usbcali.estadistica.utils.Utilidades;

@ManagedBean
@ViewScoped
public class MuestreoAleatorioEstratificadoVista {
	
	private final static Logger log=LoggerFactory.getLogger(MuestreoAleatorioEstratificadoVista.class);
	
	@Autowired
	private MuestreoAleatorioSimpleLogica masLogica;
	
	private InputText txtTotalDatos;
	private InputText txtNumeroMuestras;
	
	private CommandButton btnLimpiar;
	private CommandButton btnCalcular;
	
	private InputTextarea txtAreaDatos;
	private Panel pnlDatos;
	
	public String calcularAction() {
		log.info("Calcular Action");
		try {
			masLogica = new MuestreoAleatorioSimpleLogica();
			log.info(txtAreaDatos.getValue().toString());
			masLogica.setDatos(Utilidades.obtenerDatosString(txtAreaDatos.getValue().toString()));
			txtTotalDatos.setValue(masLogica.getTotalDatos());
			
			btnLimpiar.setDisabled(false);
		} catch (Exception e) {
			log.info("Error: "+e.getMessage());
			FacesUtils.addErrorMessage(e.getMessage());
		}
		return "";
	}
	
	public String crearInputsTamanoMuestras() throws Exception {
		try {
			
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		return "";
	}
	
	public String limpiarAction() {
		txtTotalDatos.resetValue();
		
		btnLimpiar.setDisabled(true);
		
		txtAreaDatos.resetValue();
		
		return "";
	}
	
	/**
	 * Inicio Getters and Setters
	 */
	public InputText getTxtTotalDatos() {
		return txtTotalDatos;
	}

	public void setTxtTotalDatos(InputText txtTotalDatos) {
		this.txtTotalDatos = txtTotalDatos;
	}

	public CommandButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(CommandButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}

	public CommandButton getBtnCalcular() {
		return btnCalcular;
	}

	public void setBtnCalcular(CommandButton btnCalcular) {
		this.btnCalcular = btnCalcular;
	}

	public InputTextarea getTxtAreaDatos() {
		return txtAreaDatos;
	}

	public void setTxtAreaDatos(InputTextarea txtAreaDatos) {
		this.txtAreaDatos = txtAreaDatos;
	}
	
	public InputText getTxtNumeroMuestras() {
		return txtNumeroMuestras;
	}
	
	public void setTxtNumeroMuestras(InputText txtNumeroMuestras) {
		this.txtNumeroMuestras = txtNumeroMuestras;
	}

	public Panel getPnlDatos() {
		return pnlDatos;
	}

	public void setPnlDatos(Panel pnlDatos) {
		this.pnlDatos = pnlDatos;
	}
	/**
	 * Fin Getters and Setters
	 */
}
