package co.edu.usbcali.estadistica.vista;

import java.text.DecimalFormat;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.outputlabel.OutputLabel;
import org.springframework.beans.factory.annotation.Autowired;

import co.edu.usbcali.estadistica.logica.RazonVarianzasLogica;
import co.edu.usbcali.estadistica.utils.FacesUtils;
import co.edu.usbcali.estadistica.utils.Utilidades;

@ManagedBean
@ViewScoped
public class ICRazonVarianzasVista {

	@Autowired
	private RazonVarianzasLogica rv;
	
	private InputNumber numTamanoMuestraPob1;
	private InputNumber numTamanoMuestraPob2;
	private InputNumber numMediaMuestralPob1;
	private InputNumber numMediaMuestralPob2;
	private InputNumber numVarianzaMuestralPob1;
	private InputNumber numVarianzaMuestralPob2;
	private InputNumber numNivelConfianza;
	private InputNumber numAlfa;
	
	private InputNumber numFisherAlfaMedios;
	private InputNumber numFisherAlfaMediosMenos;
	private InputNumber numGradoslibertadPob1;
	private InputNumber numGradoslibertadPob2;
	private OutputLabel oplICTitulo;
	private OutputLabel oplIC;
	private OutputLabel oplInterpretacion;
	
	private CommandButton btnCalcular;
	private CommandButton btnLimpiar;
	
	public String calcular() {
		try {
			if(numTamanoMuestraPob1.getValue() == null || numTamanoMuestraPob2.getValue() == null || numVarianzaMuestralPob1.getValue() == null || numVarianzaMuestralPob2.getValue() == null || numNivelConfianza.getValue() == null) {
				throw new Exception("Ingrese todos los campos");
			}
			rv = new RazonVarianzasLogica();
			
			rv.setDesviacionEstandarMuestralPob1(Utilidades.convertirtxtValorDouble(numVarianzaMuestralPob1.getValue().toString()));
			rv.setDesviacionEstandarMuestralPob2(Utilidades.convertirtxtValorDouble(numVarianzaMuestralPob2.getValue().toString()));
			rv.setNivelConfianza(Utilidades.convertirtxtValorDouble(numNivelConfianza.getValue().toString()));
			rv.setTamañoPob1(Utilidades.convertirStringToInt(numTamanoMuestraPob1.getValue().toString()));
			rv.setTamañoPob2(Utilidades.convertirStringToInt(numTamanoMuestraPob2.getValue().toString()));
			
			rv.calcular();
			
			DecimalFormat df = new DecimalFormat("#.00");
			
			numFisherAlfaMedios.setValue(rv.getFisherAlfaMedios());
			numFisherAlfaMediosMenos.setValue(1-rv.getFisherAlfaMedios());
			numGradoslibertadPob1.setValue(rv.getGradosLibertad1());
			numGradoslibertadPob2.setValue(rv.getGradosLibertad2());
			oplIC.setValue("("+df.format(rv.getLimInferior())+" ; "+df.format(rv.getLimSuperior())+")");
			oplInterpretacion.setValue(rv.getInterpretacion());
			
		} catch (Exception e) {
			FacesUtils.addErrorMessage(e.getMessage());
		}
		return "";
	}
	
	public String limpiar() {
try {
			
		} catch (Exception e) {
			FacesUtils.addErrorMessage(e.getMessage());
		}
		return "";
	}
	
	public InputNumber getNumTamanoMuestraPob1() {
		return numTamanoMuestraPob1;
	}
	public void setNumTamanoMuestraPob1(InputNumber numTamanoMuestraPob1) {
		this.numTamanoMuestraPob1 = numTamanoMuestraPob1;
	}
	public InputNumber getNumTamanoMuestraPob2() {
		return numTamanoMuestraPob2;
	}
	public void setNumTamanoMuestraPob2(InputNumber numTamanoMuestraPob2) {
		this.numTamanoMuestraPob2 = numTamanoMuestraPob2;
	}
	public InputNumber getNumMediaMuestralPob1() {
		return numMediaMuestralPob1;
	}
	public void setNumMediaMuestralPob1(InputNumber numMediaMuestralPob1) {
		this.numMediaMuestralPob1 = numMediaMuestralPob1;
	}
	public InputNumber getNumMediaMuestralPob2() {
		return numMediaMuestralPob2;
	}
	public void setNumMediaMuestralPob2(InputNumber numMediaMuestralPob2) {
		this.numMediaMuestralPob2 = numMediaMuestralPob2;
	}
	public InputNumber getNumVarianzaMuestralPob1() {
		return numVarianzaMuestralPob1;
	}
	public void setNumVarianzaMuestralPob1(InputNumber numVarianzaMuestralPob1) {
		this.numVarianzaMuestralPob1 = numVarianzaMuestralPob1;
	}
	public InputNumber getNumVarianzaMuestralPob2() {
		return numVarianzaMuestralPob2;
	}
	public void setNumVarianzaMuestralPob2(InputNumber numVarianzaMuestralPob2) {
		this.numVarianzaMuestralPob2 = numVarianzaMuestralPob2;
	}
	public InputNumber getNumNivelConfianza() {
		return numNivelConfianza;
	}
	public void setNumNivelConfianza(InputNumber numNivelConfianza) {
		this.numNivelConfianza = numNivelConfianza;
	}
	public InputNumber getNumAlfa() {
		return numAlfa;
	}
	public void setNumAlfa(InputNumber numAlfa) {
		this.numAlfa = numAlfa;
	}
	public InputNumber getNumFisherAlfaMedios() {
		return numFisherAlfaMedios;
	}
	public void setNumFisherAlfaMedios(InputNumber numFisherAlfaMedios) {
		this.numFisherAlfaMedios = numFisherAlfaMedios;
	}
	public InputNumber getNumFisherAlfaMediosMenos() {
		return numFisherAlfaMediosMenos;
	}
	public void setNumFisherAlfaMediosMenos(InputNumber numFisherAlfaMediosMenos) {
		this.numFisherAlfaMediosMenos = numFisherAlfaMediosMenos;
	}
	public InputNumber getNumGradoslibertadPob1() {
		return numGradoslibertadPob1;
	}
	public void setNumGradoslibertadPob1(InputNumber numGradoslibertadPob1) {
		this.numGradoslibertadPob1 = numGradoslibertadPob1;
	}
	public InputNumber getNumGradoslibertadPob2() {
		return numGradoslibertadPob2;
	}
	public void setNumGradoslibertadPob2(InputNumber numGradoslibertadPob2) {
		this.numGradoslibertadPob2 = numGradoslibertadPob2;
	}
	public OutputLabel getOplICTitulo() {
		return oplICTitulo;
	}
	public void setOplICTitulo(OutputLabel oplICTitulo) {
		this.oplICTitulo = oplICTitulo;
	}
	public OutputLabel getOplIC() {
		return oplIC;
	}
	public void setOplIC(OutputLabel oplIC) {
		this.oplIC = oplIC;
	}
	public OutputLabel getOplInterpretacion() {
		return oplInterpretacion;
	}
	public void setOplInterpretacion(OutputLabel oplInterpretacion) {
		this.oplInterpretacion = oplInterpretacion;
	}
	public CommandButton getBtnCalcular() {
		return btnCalcular;
	}
	public void setBtnCalcular(CommandButton btnCalcular) {
		this.btnCalcular = btnCalcular;
	}
	public CommandButton getBtnLimpiar() {
		return btnLimpiar;
	}
	public void setBtnLimpiar(CommandButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}
	
}
