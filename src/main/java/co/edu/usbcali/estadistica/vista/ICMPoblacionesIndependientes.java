package co.edu.usbcali.estadistica.vista;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.panelgrid.PanelGrid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import co.edu.usbcali.estadistica.logica.IntervaloConfianza;
import co.edu.usbcali.estadistica.utils.FacesUtils;

@ManagedBean
@ViewScoped
public class ICMPoblacionesIndependientes {
private final static Logger log=LoggerFactory.getLogger(ICMPoblacionesIndependientes.class);
	
	@Autowired
	private IntervaloConfianza intervaloConfianza;
	
	private InputNumber numDesviacionEstandarPob1;
	private InputNumber numDesviacionEstandarPob2;
	private InputNumber numMediaMuestralPob1;
	private InputNumber numMediaMuestralPob2;
	private InputNumber numTamanoMuestraPob1;
	private InputNumber numTamanoMuestraPob2;
	private InputNumber numNivelConfianza;
	private InputNumber numAlfa;
	private OutputLabel oplCaso;
	private PanelGrid pnlResultado;
	
	private CommandButton btnLimpiar;
	private CommandButton btnCalcular;
	
	public double[] resultadoIntervalo;
		
	public String calcularAction() {
		log.info("Calcular Action");
		try {
			
			
		} catch (Exception e) {
			FacesUtils.addErrorMessage(e.getMessage());
		}
		return "";
	}
	
	public String limpiarAction() {
		
			
		return "";
	}

	public IntervaloConfianza getIntervaloConfianza() {
		return intervaloConfianza;
	}

	public void setIntervaloConfianza(IntervaloConfianza intervaloConfianza) {
		this.intervaloConfianza = intervaloConfianza;
	}

	public InputNumber getNumDesviacionEstandarPob1() {
		return numDesviacionEstandarPob1;
	}

	public void setNumDesviacionEstandarPob1(InputNumber numDesviacionEstandarPob1) {
		this.numDesviacionEstandarPob1 = numDesviacionEstandarPob1;
	}

	public InputNumber getNumDesviacionEstandarPob2() {
		return numDesviacionEstandarPob2;
	}

	public void setNumDesviacionEstandarPob2(InputNumber numDesviacionEstandarPob2) {
		this.numDesviacionEstandarPob2 = numDesviacionEstandarPob2;
	}

	public InputNumber getNumMediaMuestralPob1() {
		return numMediaMuestralPob1;
	}

	public void setNumMediaMuestralPob1(InputNumber numMediaMuestralPob1) {
		this.numMediaMuestralPob1 = numMediaMuestralPob1;
	}

	public InputNumber getNumMediaMuestralPob2() {
		return numMediaMuestralPob2;
	}

	public void setNumMediaMuestralPob2(InputNumber numMediaMuestralPob2) {
		this.numMediaMuestralPob2 = numMediaMuestralPob2;
	}

	public InputNumber getNumTamanoMuestraPob1() {
		return numTamanoMuestraPob1;
	}

	public void setNumTamanoMuestraPob1(InputNumber numTamanoMuestraPob1) {
		this.numTamanoMuestraPob1 = numTamanoMuestraPob1;
	}

	public InputNumber getNumTamanoMuestraPob2() {
		return numTamanoMuestraPob2;
	}

	public void setNumTamanoMuestraPob2(InputNumber numTamanoMuestraPob2) {
		this.numTamanoMuestraPob2 = numTamanoMuestraPob2;
	}

	public InputNumber getNumNivelConfianza() {
		return numNivelConfianza;
	}

	public void setNumNivelConfianza(InputNumber numNivelConfianza) {
		this.numNivelConfianza = numNivelConfianza;
	}

	public InputNumber getNumAlfa() {
		return numAlfa;
	}

	public void setNumAlfa(InputNumber numAlfa) {
		this.numAlfa = numAlfa;
	}

	public OutputLabel getOplCaso() {
		return oplCaso;
	}

	public void setOplCaso(OutputLabel oplCaso) {
		this.oplCaso = oplCaso;
	}
	
	public PanelGrid getPnlResultado() {
		return pnlResultado;
	}

	public void setPnlResultado(PanelGrid pnlResultado) {
		this.pnlResultado = pnlResultado;
	}

	public CommandButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(CommandButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}

	public CommandButton getBtnCalcular() {
		return btnCalcular;
	}

	public void setBtnCalcular(CommandButton btnCalcular) {
		this.btnCalcular = btnCalcular;
	}

	public double[] getResultadoIntervalo() {
		return resultadoIntervalo;
	}

	public void setResultadoIntervalo(double[] resultadoIntervalo) {
		this.resultadoIntervalo = resultadoIntervalo;
	}

}
