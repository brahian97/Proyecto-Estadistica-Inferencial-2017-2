package co.edu.usbcali.estadistica.vista;

import java.text.DecimalFormat;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import co.edu.usbcali.estadistica.logica.Estimador;
import co.edu.usbcali.estadistica.utils.FacesUtils;
import co.edu.usbcali.estadistica.utils.Utilidades;

@ManagedBean
@ViewScoped
public class EstimadoresVista {
	
	private final static Logger log=LoggerFactory.getLogger(EstimadoresVista.class);
	
	@Autowired
	private Estimador estimador;
	
	private InputText txtMediaMuestral;
	private InputText txtModaMuestral;
	private InputText txtMedianaMuestral;
	private InputText txtTotalDatos;
	private InputText txtVarianzaMuestral;
	private InputText txtDesviacionEstandarMuestral;
	
	private CommandButton btnLimpiar;
	private CommandButton btnCalcular;
	
	private InputTextarea txtAreaDatos;
	
	public String calcularAction() {
		log.info("Calcular Action");
		try {
			estimador = new Estimador();
			DecimalFormat df = new DecimalFormat("#.00");
			log.info(txtAreaDatos.getValue().toString());
			estimador.setDatos(Utilidades.obtenerDatosDouble(txtAreaDatos.getValue().toString()));
			txtTotalDatos.setValue(estimador.getTotalDatos());
			txtMediaMuestral.setValue(estimador.getMedia());
			txtVarianzaMuestral.setValue(estimador.getVarianza());
			txtDesviacionEstandarMuestral.setValue(df.format(estimador.getDesviacionEstandar()));
			txtModaMuestral.setValue(estimador.getModa());
			String mediana="";
			for(int i=0; i<estimador.getMediana().length; i++) {
				mediana = String.valueOf(estimador.getMediana()[i]);
				mediana.concat(" ,");
			}
			txtMedianaMuestral.setValue(mediana.substring(0, mediana.length()-1));
			btnLimpiar.setDisabled(false);
		} catch (Exception e) {
			log.info("Error: "+e.getMessage());
			FacesUtils.addErrorMessage(e.getMessage());
		}
		return "";
	}
	
	public String limpiarAction() {
		txtMediaMuestral.resetValue();
		txtModaMuestral.resetValue();
		txtMedianaMuestral.resetValue();
		txtTotalDatos.resetValue();
		txtVarianzaMuestral.resetValue();
		txtDesviacionEstandarMuestral.resetValue();
		
		btnLimpiar.setDisabled(true);
		
		txtAreaDatos.resetValue();
		
		return "";
	}
	
	/**
	 * Inicio Getters and Setters
	 */
	public InputText getTxtMediaMuestral() {
		return txtMediaMuestral;
	}

	public void setTxtMediaMuestral(InputText txtMediaMuestral) {
		this.txtMediaMuestral = txtMediaMuestral;
	}

	public InputText getTxtModaMuestral() {
		return txtModaMuestral;
	}

	public void setTxtModaMuestral(InputText txtModaMuestral) {
		this.txtModaMuestral = txtModaMuestral;
	}

	public InputText getTxtMedianaMuestral() {
		return txtMedianaMuestral;
	}

	public void setTxtMedianaMuestral(InputText txtMedianaMuestral) {
		this.txtMedianaMuestral = txtMedianaMuestral;
	}

	public InputText getTxtTotalDatos() {
		return txtTotalDatos;
	}

	public void setTxtTotalDatos(InputText txtTotalDatos) {
		this.txtTotalDatos = txtTotalDatos;
	}

	public InputText getTxtVarianzaMuestral() {
		return txtVarianzaMuestral;
	}

	public void setTxtVarianzaMuestral(InputText txtVarianzaMuestral) {
		this.txtVarianzaMuestral = txtVarianzaMuestral;
	}

	public InputText getTxtDesviacionEstandarMuestral() {
		return txtDesviacionEstandarMuestral;
	}

	public void setTxtDesviacionEstandarMuestral(InputText txtDesviacionEstandarMuestral) {
		this.txtDesviacionEstandarMuestral = txtDesviacionEstandarMuestral;
	}

	public CommandButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(CommandButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}

	public CommandButton getBtnCalcular() {
		return btnCalcular;
	}

	public void setBtnCalcular(CommandButton btnCalcular) {
		this.btnCalcular = btnCalcular;
	}

	public InputTextarea getTxtAreaDatos() {
		return txtAreaDatos;
	}

	public void setTxtAreaDatos(InputTextarea txtAreaDatos) {
		this.txtAreaDatos = txtAreaDatos;
	}
	/**
	 * Fin Getters and Setters
	 */
}
