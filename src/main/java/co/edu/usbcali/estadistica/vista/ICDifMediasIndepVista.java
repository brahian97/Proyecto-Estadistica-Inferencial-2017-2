package co.edu.usbcali.estadistica.vista;

import org.primefaces.component.inputtextarea.InputTextarea;

import java.text.DecimalFormat;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.panelgrid.PanelGrid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import co.edu.usbcali.estadistica.logica.ICDifMediasIndp;
import co.edu.usbcali.estadistica.utils.FacesUtils;
import co.edu.usbcali.estadistica.utils.Utilidades;
import jsat.clustering.OPTICS;

@ManagedBean
@ViewScoped
public class ICDifMediasIndepVista {

	private final static Logger log=LoggerFactory.getLogger(ICDifMediasIndepVista.class);
	
	@Autowired
	private ICDifMediasIndp IC;
	
	//Variables de la vista
	private InputTextarea txtAreaDatosPob1;
	private InputTextarea txtAreaDatosPob2;
	private InputNumber numNivelConfianza;
	private InputNumber numAlfa;
	private CommandButton btnCalcular;
	private CommandButton btnLimpiar;
	private PanelGrid pnlResultados;
	private OutputLabel oplZ;
	private OutputLabel oplGradosLibertad;
	private OutputLabel oplICTitulo;
	private OutputLabel oplIC;
	private OutputLabel oplInterpretacion;
	
	public String calcular() {
		try {
			if(numNivelConfianza == null || txtAreaDatosPob1 == null || txtAreaDatosPob2 == null) {
				throw new Exception("Todos los datos son requeridos");
			}
			IC = new ICDifMediasIndp();
			
			IC.setPobAntes(Utilidades.stringToDoubleAray(txtAreaDatosPob1.getValue().toString()));
			IC.setPobDespues(Utilidades.stringToDoubleAray(txtAreaDatosPob2.getValue().toString()));
			
			IC.setNivelConfianza(Utilidades.convertirtxtValorDouble(numNivelConfianza.getValue().toString()));
			
			IC.calcular();
			DecimalFormat df = new DecimalFormat("#.00");
			oplGradosLibertad.setValue(df.format(IC.getGradosLibertad()));
			oplZ.setValue(df.format(Utilidades.calcularTAlfaMedios(IC.getGradosLibertad(), IC.getNivelConfianza())));
			log.info("En la vista -----"+IC.getLimInferior());
			oplIC.setValue("( "+df.format(IC.getLimInferior())+" ; "+df.format(IC.getLimSuperior())+")"); 
			pnlResultados.setStyle("display: block;");
			
			oplInterpretacion.setValue(IC.getInterpretacion());
			
		} catch (Exception e) {
			FacesUtils.addErrorMessage(e.getMessage());
		}
		return "";
	}
	
	public String limpiar() {
		try {
			
		} catch (Exception e) {
			FacesUtils.addErrorMessage(e.getMessage());
		}
		return "";
	}
	
	public ICDifMediasIndp getIC() {
		return IC;
	}
	public void setIC(ICDifMediasIndp iC) {
		IC = iC;
	}
	public InputTextarea getTxtAreaDatosPob1() {
		return txtAreaDatosPob1;
	}
	public void setTxtAreaDatosPob1(InputTextarea txtAreaDatosPob1) {
		this.txtAreaDatosPob1 = txtAreaDatosPob1;
	}
	public InputTextarea getTxtAreaDatosPob2() {
		return txtAreaDatosPob2;
	}
	public void setTxtAreaDatosPob2(InputTextarea txtAreaDatosPob2) {
		this.txtAreaDatosPob2 = txtAreaDatosPob2;
	}
	public InputNumber getNumNivelConfianza() {
		return numNivelConfianza;
	}
	public void setNumNivelConfianza(InputNumber numNivelConfianza) {
		this.numNivelConfianza = numNivelConfianza;
	}
	public InputNumber getNumAlfa() {
		return numAlfa;
	}
	public void setNumAlfa(InputNumber numAlfa) {
		this.numAlfa = numAlfa;
	}
	public CommandButton getBtnCalcular() {
		return btnCalcular;
	}
	public void setBtnCalcular(CommandButton btnCalcular) {
		this.btnCalcular = btnCalcular;
	}
	public CommandButton getBtnLimpiar() {
		return btnLimpiar;
	}
	public void setBtnLimpiar(CommandButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}
	public PanelGrid getPnlResultados() {
		return pnlResultados;
	}
	public void setPnlResultados(PanelGrid pnlResultados) {
		this.pnlResultados = pnlResultados;
	}
	public OutputLabel getOplZ() {
		return oplZ;
	}
	public void setOplZ(OutputLabel oplZ) {
		this.oplZ = oplZ;
	}
	public OutputLabel getOplGradosLibertad() {
		return oplGradosLibertad;
	}
	public void setOplGradosLibertad(OutputLabel oplGradosLibertad) {
		this.oplGradosLibertad = oplGradosLibertad;
	}
	public OutputLabel getOplICTitulo() {
		return oplICTitulo;
	}
	public void setOplICTitulo(OutputLabel oplICTitulo) {
		this.oplICTitulo = oplICTitulo;
	}
	public OutputLabel getOplIC() {
		return oplIC;
	}
	public void setOplIC(OutputLabel oplIC) {
		this.oplIC = oplIC;
	}
	public OutputLabel getOplInterpretacion() {
		return oplInterpretacion;
	}
	public void setOplInterpretacion(OutputLabel oplInterpretacion) {
		this.oplInterpretacion = oplInterpretacion;
	}
	
}
