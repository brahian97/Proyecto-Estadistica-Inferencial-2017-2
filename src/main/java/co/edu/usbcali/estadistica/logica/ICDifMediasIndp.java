package co.edu.usbcali.estadistica.logica;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.usbcali.estadistica.utils.Utilidades;

public class ICDifMediasIndp {
	
	private final static Logger log = LoggerFactory.getLogger(Utilidades.class);


	private double difMediaPob;
	private int tamano;
	private double desviacionEstandard;
	private double mediad;
	private double nivelConfianza;
	private double alfa;
	private double limInferior;
	private double limSuperior;
	private double gradosLibertad;
	private double[] pobAntes;
	private double[] pobDespues;
	private double[] d;
	private double tAlfaMedios;
	private String interpretacion = null;
	private int tamanoPob1;
	private int tamanoPob2;

	public void calcular() throws Exception {
		try {
			if (tamano > 30 || tamano < 0) {
				throw new Exception("El tamaño de la muestra debe ser menor a 30");
			}
			if (nivelConfianza > 1) {
				throw new Exception("No se puede tener un nivel de confianza mayor a 100%");
			}
			
			if (tamanoPob1 != tamanoPob2) {
				throw new Exception("Los tamaños de las muestras deben ser iguales");
			}
			tamano = tamanoPob1;
			if(tamano == 1) {
				throw new Exception("El tamaño de la muestra debe ser mayor a uno");
			}
			calcularMediad();
			calcualarDesviaciond();
			gradosLibertad = tamano-1;
			
			log.info(""+gradosLibertad);

			tAlfaMedios = Utilidades.calcularTAlfaMedios(gradosLibertad, nivelConfianza);
			log.info("Media: "+mediad+"  Desviacion: "+desviacionEstandard);
			limInferior = mediad - tAlfaMedios * (desviacionEstandard / Math.sqrt(tamano));
			limSuperior = mediad + tAlfaMedios * (desviacionEstandard / Math.sqrt(tamano));
			log.info("En logica --- "+limInferior);
			
			interpretar();

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	private void interpretar() throws Exception {
		try {
			if (limInferior > 1 && limSuperior > 1) {
				interpretacion = "Con un nivel de confianza del "+ nivelConfianza*100 +"%, se puede afirmar que la media poblacional de la población 1 es mayor a la de la población 2";
			}else {
				if (limInferior < 0 && limSuperior < 0) {
					interpretacion = "Con un nivel de confianza del "+ nivelConfianza*100 +"%, se puede afirmar que la media poblacional de la población 2 es mayor a la de la población 1";
				}else {
					if (limInferior < 0 && limSuperior >= 0) {
						interpretacion = "Con los muestreos realizados no se puede afirmar que existan difrencias significativas entre las medias de las dos poblaciones";
					}
				}
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	private void calcularMediad() throws Exception {
		try {
			d = new double[(int) tamano];
			double aux;
			for (int i = 0; i < tamano; i++) {
				aux = pobAntes[i] - pobDespues[i];
				d[i] = aux;
				mediad += aux;
			}
			mediad = mediad / tamano;

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	private void calcualarDesviaciond() throws Exception {
		try {
			d = new double[(int) tamano];
			double aux;
			for (int i = 0; i < tamano; i++) {
				aux = Math.pow((d[i] - mediad), 2);
				desviacionEstandard += aux;
			}
			desviacionEstandard = desviacionEstandard / (tamano - 1);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public double getDifMediaPob() {
		return difMediaPob;
	}

	public void setDifMediaPob(double difMediaPob) {
		this.difMediaPob = difMediaPob;
	}

	public double getTamano() {
		return tamano;
	}

	public void setTamano(int tamano) {
		this.tamano = tamano;
		this.gradosLibertad = tamano - 1;
	}

	public double getDesviacionEstandard() {
		return desviacionEstandard;
	}

	public void setDesviacionEstandard(double desviacionEstandard) {
		this.desviacionEstandard = desviacionEstandard;
	}

	public double getMediad() {
		return mediad;
	}

	public double getNivelConfianza() {
		return nivelConfianza;
	}

	public void setNivelConfianza(double nivelConfianza) {
		this.nivelConfianza = nivelConfianza;
		this.alfa = 1 - nivelConfianza;
	}

	public double getAlfa() {
		return alfa;
	}

	public double getLimInferior() {
		return limInferior;
	}

	public double getLimSuperior() {
		return limSuperior;
	}

	public double getGradosLibertad() {
		return gradosLibertad;
	}

	public double[] getPobAntes() {
		return pobAntes;
	}

	public void setPobAntes(double[] pobAntes) {
		this.pobAntes = pobAntes;
		this.tamanoPob1 = pobAntes.length;
	}

	public double[] getPobDespues() {
		return pobDespues;
	}

	public void setPobDespues(double[] pobDespues) {
		this.pobDespues = pobDespues;
		this.tamanoPob2 = pobDespues.length;
	}

	public double gettAlfaMedios() {
		return tAlfaMedios;
	}

	public String getInterpretacion() {
		return interpretacion;
	}
}
