package co.edu.usbcali.estadistica.logica;

import org.apache.commons.math3.distribution.TDistribution;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jdistlib.*;

public class IntervaloConfianza {
	private static final Logger log = LoggerFactory.getLogger(IntervaloConfianza.class);

	private Double varianzaPob;
	private Double varianzaMue;
	private Double muestra;
	private Double mediaMuestral;
	private Double zAlfaMedios;
	private Double nivelConfianza;
	private Double p;
	private Double q;
	private Double x;
	private Double critVal;
	private double limInferior;
	private double limSuperior;
	private double[] intervaloMedia;
	private double[] intervaloProporcion;
	private boolean isCase1 = false;

	private double calcularZAlfaMedios(double nivelConfianza) throws Exception {
		try {
			if (nivelConfianza == 0.70) {
				zAlfaMedios = 1.04;
				return zAlfaMedios;
			}
			if (nivelConfianza == 0.75) {
				zAlfaMedios = 1.15;
				return zAlfaMedios;
			}
			if (nivelConfianza == 0.80) {
				zAlfaMedios = 1.28;
				return zAlfaMedios;
			}
			if (nivelConfianza == 0.85) {
				zAlfaMedios = 1.44;
				return zAlfaMedios;
			}
			if (nivelConfianza == 0.90) {
				zAlfaMedios = 1.645;
				return zAlfaMedios;
			}
			if (nivelConfianza == 0.91) {
				zAlfaMedios = 1.69;
				return zAlfaMedios;
			}
			if (nivelConfianza == 0.92) {
				zAlfaMedios = 1.75;
				return zAlfaMedios;
			}
			if (nivelConfianza == 0.93) {
				zAlfaMedios = 1.81;
				return zAlfaMedios;
			}
			if (nivelConfianza == 0.94) {
				zAlfaMedios = 1.88;
				return zAlfaMedios;
			}
			if (nivelConfianza == 0.95) {
				zAlfaMedios = 1.96;
				return zAlfaMedios;
			}
			if (nivelConfianza == 0.96) {
				zAlfaMedios = 2.05;
				return zAlfaMedios;
			}
			if (nivelConfianza == 0.97) {
				zAlfaMedios = 2.17;
				return zAlfaMedios;
			}
			if (nivelConfianza == 0.98) {
				zAlfaMedios = 2.33;
				return zAlfaMedios;
			}
			if (nivelConfianza == 0.99) {
				zAlfaMedios = 2.58;
				return zAlfaMedios;
			}
			return -1;
		} catch (Exception e) {
			return 0D;
		}
	}

	private double calcularTAlfaMedios(double muestra, double nivelC) {
		try {
			// Create T Distribution with N-1 degrees of freedom
			TDistribution tDist = new TDistribution(muestra - 1);
			// Calculate critical value
			this.critVal = tDist.inverseCumulativeProbability(1.0 - (1 - nivelC) / 2);
			// Calculate confidence interval
			return this.critVal;
		} catch (MathIllegalArgumentException e) {
			return Double.NaN;
		}
	}

	public void CalcularIntervaloMedia() throws Exception {
	
		log.info(""+mediaMuestral+"-"+nivelConfianza+"-"+varianzaPob+"-"+muestra+"-"+varianzaMue);
		
		if (muestra >= 30 || varianzaPob > 0) {
			double nc = calcularZAlfaMedios(nivelConfianza);
			this.limInferior = (mediaMuestral - (nc*(varianzaPob/Math.sqrt(muestra))));
			this.limSuperior = (mediaMuestral + (nc*(varianzaPob/Math.sqrt(muestra))));
			this.isCase1 = true;
		}
		if (muestra < 30 && varianzaPob == 0.0) {
			log.info(""+calcularTAlfaMedios(muestra, nivelConfianza));
			this.limInferior = (mediaMuestral - (calcularTAlfaMedios(muestra, nivelConfianza))
					* (varianzaMue / Math.sqrt(muestra)));
			this.limSuperior = (mediaMuestral + (calcularTAlfaMedios(muestra, nivelConfianza))
					* (varianzaMue / Math.sqrt(muestra)));
			this.isCase1 = false;
		}
	}

	public void calcularIntervaloProporcion() throws Exception {

		//p = x / muestra;
		q = 1 - p;

		if (muestra >= 30) {
			this.limInferior = (p - (calcularZAlfaMedios(nivelConfianza) * Math.sqrt((p * q) / muestra)));
			this.limSuperior = (p + (calcularZAlfaMedios(nivelConfianza) * Math.sqrt((p * q) / muestra)));
		}

	}

	public Double getVarianzaPob() {
		return varianzaPob;
	}

	public void setVarianzaPob(Double varianzaPob) {
		this.varianzaPob = varianzaPob;
	}

	public Double getVarianzaMue() {
		return varianzaMue;
	}

	public void setVarianzaMue(Double varianzaMue) {
		this.varianzaMue = varianzaMue;
	}

	public Double getMuestra() {
		return muestra;
	}

	public void setMuestra(Double muestra) {
		this.muestra = muestra;
	}

	public Double getMediaMuestral() {
		return mediaMuestral;
	}

	public void setMediaMuestral(Double mediaMuestral) {
		this.mediaMuestral = mediaMuestral;
	}

	public Double getzAlfaMedios() {
		return zAlfaMedios;
	}

	public void setzAlfaMedios(Double zAlfaMedios) {
		this.zAlfaMedios = zAlfaMedios;
	}

	public Double getNivelConfianza() {
		return nivelConfianza;
	}

	public void setNivelConfianza(Double nivelConfianza) {
		this.nivelConfianza = nivelConfianza;
	}

	public Double getP() {
		return p;
	}

	public void setP(Double p) {
		this.p = p;
	}

	public Double getQ() {
		return q;
	}

	public void setQ(Double q) {
		this.q = q;
	}

	public Double getX() {
		return x;
	}

	public void setX(Double x) {
		this.x = x;
	}

	public double getLimInferior() {
		return limInferior;
	}

	public void setLimInferior(double limInferior) {
		this.limInferior = limInferior;
	}

	public double getLimSuperior() {
		return limSuperior;
	}

	public void setLimSuperior(double limSuperior) {
		this.limSuperior = limSuperior;
	}

	public double[] getIntervaloMedia() {
		return intervaloMedia;
	}

	public void setIntervaloMedia(double[] intervaloMedia) {
		this.intervaloMedia = intervaloMedia;
	}

	public double[] getIntervaloProporcion() {
		return intervaloProporcion;
	}

	public void setIntervaloProporcion(double[] intervaloProporcion) {
		this.intervaloProporcion = intervaloProporcion;
	}

	public Double getCritVal() {
		return critVal;
	}

	public void setCritVal(Double critVal) {
		this.critVal = critVal;
	}

	public boolean isCase1() {
		return isCase1;
	}

	

}
