package co.edu.usbcali.estadistica.logica;

import org.apache.commons.math3.distribution.FDistribution;
import org.apache.commons.math3.distribution.TDistribution;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntervaloDosPoblaciones {
	private static final Logger log = LoggerFactory.getLogger(IntervaloConfianza.class);

	private double muestra;
	private double muestra1;
	private double muestra2;
	private double mediaMuestral1;
	private double mediaMuestral2;
	private double varianza1;
	private double varianza2;
	private double varianzaMuestral1;
	private double varianzaMuestral2;
	private double zAlfaMedios;
	private double tAlfaMedios;
	private double nivelConfianza;
	private double v;


	private double[] tipoPoblaciones;
	private double[] resultadoIntervalo;

	public double calcularZAlfaMedios(double nivelConfianza) throws Exception {
		try {
			if (nivelConfianza == 0.70) {
				zAlfaMedios = 1.04;
			}
			if (nivelConfianza == 0.75) {
				zAlfaMedios = 1.15;
			}
			if (nivelConfianza == 0.80) {
				zAlfaMedios = 1.28;
			}
			if (nivelConfianza == 0.85) {
				zAlfaMedios = 1.44;
			}
			if (nivelConfianza == 0.90) {
				zAlfaMedios = 1.645;
			}
			if (nivelConfianza == 0.91) {
				zAlfaMedios = 1.69;
			}
			if (nivelConfianza == 0.92) {
				zAlfaMedios = 1.75;
			}
			if (nivelConfianza == 0.93) {
				zAlfaMedios = 1.81;
			}
			if (nivelConfianza == 0.94) {
				zAlfaMedios = 1.88;
			}
			if (nivelConfianza == 0.95) {
				zAlfaMedios = 1.96;
			}
			if (nivelConfianza == 0.96) {
				zAlfaMedios = 2.05;
			}
			if (nivelConfianza == 0.97) {
				zAlfaMedios = 2.17;
			}
			if (nivelConfianza == 0.98) {
				zAlfaMedios = 2.33;
			}
			if (nivelConfianza == 0.99) {
				zAlfaMedios = 2.58;
			} else {
				throw new Exception(
						"Amigo mio, ¿Por qué quieres usar un nivel que no viene siendo de confianza sino de desconfianza?");
			}
			return zAlfaMedios;
		} catch (Exception e) {
			return 0D;
		}
	}

	private static double calcularTAlfaMedios(double muestra1, double muestra2, double nivelC) {
		try {
			// Create T Distribution with N-1 degrees of freedom
			TDistribution tDist = new TDistribution(muestra1+muestra2 - 2);
			// Calculate critical value
			double critVal = tDist.inverseCumulativeProbability(1.0 - (1 - nivelC) / 2);
			return critVal;
		} catch (MathIllegalArgumentException e) {
			return Double.NaN;
		}
	}
	
	private static double calcularTAlfaMediosVarianzaDistinta(double v, double nivelC) {
		try {
			// Create T Distribution with N-1 degrees of freedom
			TDistribution tDist = new TDistribution(v);
			// Calculate critical value
			double critVal = tDist.inverseCumulativeProbability(1.0 - (1 - nivelC) / 2);
			return critVal;
		} catch (MathIllegalArgumentException e) {
			return Double.NaN;
		}
	}
	
	public static double calcularFAlfaMedios( double nivelC , double dfn , double dfd ) throws IllegalArgumentException {
			double result = 0.0D;

			if ( ( dfn > 0.0D ) && ( dfd > 0.0D ) )	{
				FDistribution fDist = new FDistribution(dfn, dfd);
				result = fDist.inverseCumulativeProbability(1.0 - (1 - nivelC) / 2);
				return result;
			}else {
				throw new IllegalArgumentException( "dfn or dfd <= 0" );
			}
		}
	
	public double[] CalcularIntervalo(double nivelC , double dfn , double dfd) throws Exception{
		tipoPoblaciones[0] = (varianzaMuestral1/varianzaMuestral2)*(1/calcularFAlfaMedios(nivelC, dfn, dfd));
		tipoPoblaciones[1] = (varianzaMuestral1/varianzaMuestral2)*(calcularFAlfaMedios(nivelC, dfn, dfd));
		if(tipoPoblaciones[0]<1 && tipoPoblaciones[1]<1){
			double v = ((varianzaMuestral1/muestra1)+(varianzaMuestral2/muestra2))/(((varianzaMuestral1/muestra1)/(muestra1-1))+((varianzaMuestral2/muestra2)/(muestra2-1)));
			resultadoIntervalo[0] = (mediaMuestral1-mediaMuestral2) - (calcularTAlfaMediosVarianzaDistinta(v, nivelC)*Math.sqrt((varianzaMuestral1/muestra1)+(varianzaMuestral2/muestra2)));
			resultadoIntervalo[1] = (mediaMuestral1-mediaMuestral2) + (calcularTAlfaMediosVarianzaDistinta(v, nivelC)*Math.sqrt((varianzaMuestral1/muestra1)+(varianzaMuestral2/muestra2)));

		}
		if(tipoPoblaciones[0]>1 && tipoPoblaciones[1]>1){
			double v = ((varianzaMuestral1/muestra1)+(varianzaMuestral2/muestra2))/(((varianzaMuestral1/muestra1)/(muestra1-1))+((varianzaMuestral2/muestra2)/(muestra2-1)));
			resultadoIntervalo[0] = (mediaMuestral1-mediaMuestral2) - (calcularTAlfaMediosVarianzaDistinta(v, nivelC)*Math.sqrt((varianzaMuestral1/muestra1)+(varianzaMuestral2/muestra2)));
			resultadoIntervalo[1] = (mediaMuestral1-mediaMuestral2) + (calcularTAlfaMediosVarianzaDistinta(v, nivelC)*Math.sqrt((varianzaMuestral1/muestra1)+(varianzaMuestral2/muestra2)));

		}
		if(tipoPoblaciones[0]>=0 && tipoPoblaciones[1]>1){
			double v = mediaMuestral1+mediaMuestral2 - 2;
			double sp = Math.sqrt((((muestra1-1)*varianzaMuestral1)+((muestra2-1)*varianzaMuestral2))/v);
			resultadoIntervalo[0] = (mediaMuestral1-mediaMuestral2) - (calcularTAlfaMedios(muestra1, muestra2, nivelC)*sp*Math.sqrt((1/muestra1)+(1/muestra2)));
			resultadoIntervalo[1] = (mediaMuestral1-mediaMuestral2) + (calcularTAlfaMedios(muestra1, muestra2, nivelC)*sp*Math.sqrt((1/muestra1)+(1/muestra2)));

		}
		else if(varianza1!=0 && varianza2!=0){
			resultadoIntervalo[0] = (mediaMuestral1-mediaMuestral2) - (calcularZAlfaMedios(nivelC)*Math.sqrt((varianza1/muestra1)+(varianza2/muestra2)));
			resultadoIntervalo[1] = (mediaMuestral1-mediaMuestral2) + (calcularZAlfaMedios(nivelC)*Math.sqrt((varianza1/muestra1)+(varianza2/muestra2)));
		}
		
		return resultadoIntervalo;
		
	}

	public double getMuestra() {
		return muestra;
	}

	public void setMuestra(double muestra) {
		this.muestra = muestra;
	}

	public double getMuestra1() {
		return muestra1;
	}

	public void setMuestra1(double muestra1) {
		this.muestra1 = muestra1;
	}

	public double getMuestra2() {
		return muestra2;
	}

	public void setMuestra2(double muestra2) {
		this.muestra2 = muestra2;
	}

	public double getMediaMuestral1() {
		return mediaMuestral1;
	}

	public void setMediaMuestral1(double mediaMuestral1) {
		this.mediaMuestral1 = mediaMuestral1;
	}

	public double getMediaMuestral2() {
		return mediaMuestral2;
	}

	public void setMediaMuestral2(double mediaMuestral2) {
		this.mediaMuestral2 = mediaMuestral2;
	}

	public double getVarianza1() {
		return varianza1;
	}

	public void setVarianza1(double varianza1) {
		this.varianza1 = varianza1;
	}

	public double getVarianza2() {
		return varianza2;
	}

	public void setVarianza2(double varianza2) {
		this.varianza2 = varianza2;
	}

	public double getVarianzaMuestral1() {
		return varianzaMuestral1;
	}

	public void setVarianzaMuestral1(double varianzaMuestral1) {
		this.varianzaMuestral1 = varianzaMuestral1;
	}

	public double getVarianzaMuestral2() {
		return varianzaMuestral2;
	}

	public void setVarianzaMuestral2(double varianzaMuestral2) {
		this.varianzaMuestral2 = varianzaMuestral2;
	}

	public double getzAlfaMedios() {
		return zAlfaMedios;
	}

	public void setzAlfaMedios(double zAlfaMedios) {
		this.zAlfaMedios = zAlfaMedios;
	}

	public double gettAlfaMedios() {
		return tAlfaMedios;
	}

	public void settAlfaMedios(double tAlfaMedios) {
		this.tAlfaMedios = tAlfaMedios;
	}

	public double[] getTipoPoblaciones() {
		return tipoPoblaciones;
	}

	public void setTipoPoblaciones(double[] tipoPoblaciones) {
		this.tipoPoblaciones = tipoPoblaciones;
	}
	

	public double[] getResultadoIntervalo() {
		return resultadoIntervalo;
	}

	public void setResultadoIntervalo(double[] resultadoIntervalo) {
		this.resultadoIntervalo = resultadoIntervalo;
	}

	public double getNivelConfianza() {
		return nivelConfianza;
	}

	public void setNivelConfianza(double nivelConfianza) {
		this.nivelConfianza = nivelConfianza;
	}

	public double getV() {
		return v;
	}

	public void setV(double v) {
		this.v = v;
	}

}
