package co.edu.usbcali.estadistica.logica;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import org.primefaces.component.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.usbcali.estadistica.utils.Utilidades;
import co.edu.usbcali.estadistica.vista.HipotesisUnaPoblacionProporcionVista;

//http://es.wikihow.com/calcular-la-varianza
public class PruebaHipotesis {

	private final static Logger log = LoggerFactory.getLogger(PruebaHipotesis.class);

	private Double hipotesisNula;
	private Double hipotesisAlterna;
	private Double desviacionEstandar;
	private Double desviacionEstandarMuestral;
	private Double media;
	private Double proporcion;
	private int tamano;
	private String simbolo;
	private Double nivelConfiabilidad;
	private int gradosLibertad;
	private double zTeorico;
	private double zCalculado;
	private String interpretación;
	private double tCalculado;
	private double tTeorico;
	private boolean useZ;

	/**
	 * Caso 0: Utilizar Z 
	 * Caso 1: Utilizar t
	 */

	private void calcularZTeorico(int caso) {

		switch (caso) {
		case 0:

			zTeorico = Utilidades.calcularZ(media, desviacionEstandar, nivelConfiabilidad);

			break;

		case 1:

			tTeorico = Utilidades.calcularTAlfaMedios(gradosLibertad, (1 - nivelConfiabilidad));

			break;
		}

	}

	/**
	 * Caso 0: distribución norma y varianza conocida Caso 1: n>=30 y varianza
	 * desconocida Caso 2: n<30, varianza desconocida Caso 3: proporción
	 * 
	 * @throws Exception
	 */

	private void calcularZCalculado(int caso) throws Exception {

		try {

			switch (caso) {
			case 0:

				zCalculado = (media - hipotesisNula) / (desviacionEstandar / (Math.sqrt(tamano)));

				break;

			case 1:

				zCalculado = (media - hipotesisNula) / (desviacionEstandar / Math.sqrt(tamano));

				break;

			case 2:
				
				tCalculado = 0;
				
				break;

			case 3:

				zCalculado = (proporcion - hipotesisNula)
						/ (Math.sqrt((hipotesisNula * (1 - hipotesisNula)) / (tamano)));

				break;
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void calcularHipotesisMedia() throws Exception {

		try {

			if (desviacionEstandar <= 0) {
				throw new Exception("Ingrese una desviación estandar valida");
			}

			if (media == null || media <= 0) {
				throw new Exception("Ingrese una media valida");
			}

			if (tamano <= 0) {
				throw new Exception("El tamaño debe ser mayor a cero");
			}

			if (nivelConfiabilidad > 0.1 || nivelConfiabilidad < 0.01 || nivelConfiabilidad == null) {
				throw new Exception("El nivel de confiabilidad debe estar entre 0.01 y 0.1");
			}

			int casoZCalculado = -1;
			int casoZTeorico = -1;

			if (tamano >= 30 && desviacionEstandar != null) {
				this.useZ = true;
				casoZCalculado = 0;
				casoZTeorico = 0;
			}

			if (tamano >= 30 && desviacionEstandar == null) {
				this.useZ = true;
				casoZCalculado = 1;
				casoZTeorico = 0;
			}

			if (tamano < 30 && desviacionEstandar == null) {
				this.useZ = false;
				casoZCalculado = 2;
				casoZTeorico = 1;
			}

			this.calcularZTeorico(casoZTeorico);
			this.calcularZCalculado(casoZCalculado);
			this.interpretar();

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void calcularHipotesisProporcion() throws Exception {

		try {

			if (proporcion == null || proporcion <= 0) {
				throw new Exception("Ingrese una proporcion valida");
			}

			if (tamano <= 0) {
				throw new Exception("El tamaño debe ser mayor a cero");
			}

			if (nivelConfiabilidad > 0.1 || nivelConfiabilidad < 0.01 || nivelConfiabilidad == null) {
				throw new Exception("El nivel de confiabilidad debe estar entre 0.01 y 0.1");
			}

			int casoZCalculado = 3;
			int casoZTeorico = 0;

			log.info("Tamaño: " + tamano + " nivel de confiabilidad: " + nivelConfiabilidad + " proporción: "
					+ proporcion + " Z teorico: " + zTeorico);

			this.gradosLibertad = tamano - 1;

			this.calcularZTeorico(casoZTeorico);
			this.calcularZCalculado(casoZCalculado);
			this.useZ = true;
			this.interpretar();

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * 0 = diferente 1 = menor que 2 = mayor que
	 */

	private void interpretar() throws Exception {
		try {

			int simbol = Integer.parseInt(simbolo);

			if (simbol == 0) {
				interpretación = "Con un nivel de significancia del " + (nivelConfiabilidad * 100)
						+ "% no hay suficiente evidencia para rechazar la hipótesis nula";
				if (this.validarEntre()) {
					interpretación = "Con un nivel de significancia del " + (nivelConfiabilidad * 100)
							+ "% se debe rechazar la hipótesis nula";
				}

			} else {
				if (simbol == 1) {
					interpretación = "Con un nivel de significancia del " + (nivelConfiabilidad * 100)
							+ "% no hay suficiente evidencia para rechazar la hipótesis nula";
					if (this.validarMenor()) {
						interpretación = "Con un nivel de significancia del " + (nivelConfiabilidad * 100)
								+ "% se debe rechazar la hipótesis nula";
					}
				} else {
					interpretación = "Con un nivel de significancia del " + (nivelConfiabilidad * 100)
							+ "% no hay suficiente evidencia para rechazar la hipótesis nula";
					if (simbol == 2) {
						if (this.validarMayor()) {
							interpretación = "Con un nivel de significancia del " + (nivelConfiabilidad * 100)
									+ "% se debe rechazar la hipótesis nula";
						}
					}
				}
			}

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * @return true, si el valor del z calculado esta en la región de rechazo de la
	 *         hipotesis nula
	 * @return false, si el valor del z calculado no esta en la región de rechazo de
	 *         la hipotesis nula
	 */
	private boolean validarEntre() {
		if (this.useZ) {
			if (zCalculado > zTeorico || zCalculado < (zTeorico * -1)) {
				return true;
			}
		} else {
			if (tCalculado > tTeorico || tCalculado < (tTeorico * -1)) {
				return true;
			}
		}

		return false;
	}

	private boolean validarMayor() {

		if (useZ) {
			if (zCalculado > zTeorico) {
				return true;
			}
		} else {
			if (tCalculado > tTeorico) {
				return true;
			}
		}

		return false;
	}

	private boolean validarMenor() {

		if (useZ) {
			if (zCalculado < (zTeorico * -1)) {
				return true;
			}
		} else {
			if (tCalculado < (tTeorico * -1)) {
				return true;
			}
		}

		return false;
	}

	public Double getHipotesisNula() {
		return hipotesisNula;
	}

	public void setHipotesisNula(Double hipotesisNula) {
		this.hipotesisNula = hipotesisNula;
	}

	public Double getHipotesisAlterna() {
		return hipotesisAlterna;
	}

	public void setHipotesisAlterna(Double hipotesisAlterna) {
		this.hipotesisAlterna = hipotesisAlterna;
	}

	public Double getDesviacionEstandar() {
		return desviacionEstandar;
	}

	public void setDesviacionEstandar(Double desviacionEstandar) {
		this.desviacionEstandar = desviacionEstandar;
	}

	public Double getMedia() {
		return media;
	}

	public void setMedia(Double media) {
		this.media = media;
	}

	public Double getProporcion() {
		return proporcion;
	}

	public void setProporcion(Double proporcion) {
		this.proporcion = proporcion;
	}

	public int getTamano() {
		return tamano;
	}

	public void setTamano(int tamano) {
		this.tamano = tamano;
	}

	public String getSimbolo() {
		return simbolo;
	}

	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}

	public Double getNivelConfiabilidad() {
		return nivelConfiabilidad;
	}

	public void setNivelConfiabilidad(Double nivelConfiabilidad) {
		this.nivelConfiabilidad = nivelConfiabilidad;
	}

	public int getGradosLibertad() {
		return gradosLibertad;
	}

	public double getzTeorico() {
		return zTeorico;
	}

	public double getzCalculado() {
		return zCalculado;
	}

	public String getInterpretacion() {
		return this.interpretación;
	}

	public double gettCalculado() {
		return tCalculado;
	}

	public double gettTeorico() {
		return tTeorico;
	}

	public boolean isUseZ() {
		return useZ;
	}

	public Double getDesviacionEstandarMuestral() {
		return desviacionEstandarMuestral;
	}

	public void setDesviacionEstandarMuestral(Double desviacionEstandarMuestral) {
		this.desviacionEstandarMuestral = desviacionEstandarMuestral;
	}
}
