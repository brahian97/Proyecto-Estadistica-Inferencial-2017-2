package co.edu.usbcali.estadistica.logica;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.usbcali.estadistica.utils.Utilidades;

public class RazonVarianzasLogica {

	private final static Logger log = LoggerFactory.getLogger(RazonVarianzasLogica.class);
	
	private double nivelConfianza;
	private int tamañoPob1;
	private int tamañoPob2;
	private int gradosLibertad1;
	private int gradosLibertad2;
	private double fisherAlfaMedios;
	private double limInferior;
	private double limSuperior;
	private double desviacionEstandarMuestralPob1;
	private double desviacionEstandarMuestralPob2;
	private String interpretacion;
	private boolean esIgual = false;
	
	public void calcular() throws Exception {
		try {
			if(nivelConfianza>1) {
				throw new Exception("El nivel de confianza no puede ser mayor a uno");
			}
			double aux = (desviacionEstandarMuestralPob1)/(desviacionEstandarMuestralPob2);
			fisherAlfaMedios = Utilidades.calcularFAlfaMedios(nivelConfianza, gradosLibertad1, gradosLibertad2);
			
			limInferior = aux*(1/fisherAlfaMedios);
			limSuperior = aux*fisherAlfaMedios;
			
			interpretar();
			
			log.info(interpretacion);
			
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	private void interpretar() throws Exception {
		try {
			if(limInferior < 1 && limSuperior < 1) {
				log.info("caso 1");
				esIgual=false;
				interpretacion = "Con una confiabilidad del "+nivelConfianza*100+"% se puede decir que la varianza de la población 2 es mayor a la de la población 1";
			}else {
				if(limInferior > 1 && limSuperior > 1) {
					log.info("caso 2");
					esIgual=false;
					interpretacion = "Con una confiabilidad del "+nivelConfianza*100+"% se puede decir que la varianza de la población 1 es mayor a la de la población 2";
				}else {
					if(limInferior >= 0 && limSuperior > 1) {
						log.info("caso 3");
						esIgual=true;
						interpretacion = "Con los muestreos realizados no se puede afirmar que existan diferencias significativas entre las varianzas de las dos poblaciones";
					}
				}
			}
			
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public double getNivelConfianza() {
		return nivelConfianza;
	}
	public void setNivelConfianza(double nivelConfianza) {
		this.nivelConfianza = nivelConfianza;
	}
	public int getTamañoPob1() {
		return tamañoPob1;
	}
	public void setTamañoPob1(int tamañoPob1) {
		this.tamañoPob1 = tamañoPob1;
		this.gradosLibertad1 = tamañoPob1-1;
	}
	public int getTamañoPob2() {
		return tamañoPob2;
	}
	public void setTamañoPob2(int tamañoPob2) {
		this.tamañoPob2 = tamañoPob2;
		this.gradosLibertad2 = tamañoPob2-1;
	}
	public int getGradosLibertad1() {
		return gradosLibertad1;
	}
	public int getGradosLibertad2() {
		return gradosLibertad2;
	}
	public double getFisherAlfaMedios() {
		return fisherAlfaMedios;
	}
	public double getLimInferior() {
		return limInferior;
	}
	public double getLimSuperior() {
		return limSuperior;
	}
	
	public double getDesviacionEstandarMuestralPob1() {
		return desviacionEstandarMuestralPob1;
	}

	public void setDesviacionEstandarMuestralPob1(double desviacionEstandarMuestralPob1) {
		this.desviacionEstandarMuestralPob1 = desviacionEstandarMuestralPob1;
	}

	public double getDesviacionEstandarMuestralPob2() {
		return desviacionEstandarMuestralPob2;
	}

	public void setDesviacionEstandarMuestralPob2(double desviacionEstandarMuestralPob2) {
		this.desviacionEstandarMuestralPob2 = desviacionEstandarMuestralPob2;
	}

	public boolean isEsIgual() {
		return esIgual;
	}

	public String getInterpretacion() {
		return interpretacion;
	}
	
}
