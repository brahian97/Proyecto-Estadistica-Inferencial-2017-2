package co.edu.usbcali.estadistica.logica;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import co.edu.usbcali.estadistica.utils.Utilidades;

//http://es.wikihow.com/calcular-la-varianza
public class Estimador {
	
	private List<Double> datos = new ArrayList<Double>();
	private Double moda = 0.0;
	private Double[] mediana;
	private Double media = 0.0;
	private Double varianza = 0.0;
	private Double desviacionEstandar = 0.0;
	private int totalDatos = 0;
	
	public Double calcularModa() {
		int maximaVecesQueSeRepite = 0;

		for(int i=0; i<totalDatos; i++){
			int vecesQueSeRepite = 0;
			for(int j=0; j<totalDatos; j++){
				if(this.datos.get(i) == this.datos.get(j)) {
					vecesQueSeRepite++;
				}
				if(vecesQueSeRepite > maximaVecesQueSeRepite){
					moda = this.datos.get(i);
					maximaVecesQueSeRepite = vecesQueSeRepite;
				}
			}
		}
		return moda;
	}
	
	private Double[] calcularMediana() {
		this.datos=Utilidades.ordenarAsc(datos);
		int mitad = (totalDatos/2);
		if(totalDatos%2!=0 && totalDatos!=1) {
			mediana = new Double[1];
			mediana[0]=datos.get(mitad);
		}else {
			if(totalDatos==1) {
				mediana = new Double[1];
				mediana[0] = datos.get(0);
			}else {
				mediana = new Double[2];
				mediana[0]=datos.get(mitad);
				mediana[1]=datos.get(mitad-1);
			}
		}
		return mediana;
	}
	
	private Double calcularMedia() throws Exception {
		try {
			double suma = 0.0;
			for(int i=0; i<totalDatos; i++) {
				suma += datos.get(i);
			}
			media = suma/totalDatos;
			return media;
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	private Double calcularVarianza() throws Exception {
		try {
			Double aux=0.0;
			for(int i=0; i<totalDatos; i++) {
				aux += Math.pow((datos.get(i)-media), 2);
			}
			
			varianza = (aux)/(totalDatos-1);
			
			return varianza;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	private Double calcularDesviacionEstandar() throws Exception {
		try {
			desviacionEstandar = Math.sqrt(varianza);
			return desviacionEstandar;
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public List<Double> getDatos() {
		return datos;
	}

	public void setDatos(List<Double> datos) throws Exception {
		try {
			this.datos = datos;
			this.totalDatos = datos.size();
			this.media = calcularMedia();
			this.mediana = calcularMediana();
			this.moda = calcularModa();
			this.varianza = calcularVarianza();
			this.desviacionEstandar = calcularDesviacionEstandar();
		}catch (Exception e) {
			throw new Exception("Error seteando datos");
		}
	}

	public Double getModa() {
		return moda;
	}

	public Double[] getMediana() {
		return mediana;
	}

	public Double getMedia() {
		return media;
	}

	public Double getVarianza() {
		return varianza;
	}

	public Double getDesviacionEstandar() {
		return desviacionEstandar;
	}

	public int getTotalDatos() {
		return totalDatos;
	}
	
	
	
}
