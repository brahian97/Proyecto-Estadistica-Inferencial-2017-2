package co.edu.usbcali.estadistica.logica;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.usbcali.estadistica.utils.Utilidades;
import co.edu.usbcali.estadistica.vista.MuestreoAleatorioSimpleVista;

//http://es.wikihow.com/calcular-la-varianza
public class MuestreoAleatorioSimpleLogica {
	
	private final static Logger log=LoggerFactory.getLogger(MuestreoAleatorioSimpleLogica.class);
	
	private List<String> datos = new ArrayList<String>();
	
	private int totalDatos = 0;
	private int tamanoMuestra;
	private int numeroMuestra;
	
	
	public String hallarMuestras() throws Exception {
		try {
			String muestras = "";
			if(tamanoMuestra>totalDatos) {
				throw new Exception("El valor de la muestra no puede ser mayor al total de datos");
			}
			int numRandom;
			
			for(int i=0; i<numeroMuestra; i++) {
				int[] array = new int[tamanoMuestra];
				boolean arrayLleno = false;
				int pos=0;
				while(!arrayLleno) {
					numRandom = (int) Math.floor(Math.random()*totalDatos);
					if(existeValor(array, numRandom)==false) {
						log.info("RANDOM="+numRandom);
						array[pos]=numRandom;
						pos++;
						log.info("POSICION="+pos);
						if(pos==array.length) {
							log.info("ARRAY LLENO");
							arrayLleno=true;
						}
					}
				}
				muestras = muestras.concat("[");
				for(int j=0; j<array.length; j++) {
					muestras = muestras.concat(datos.get(array[j])+", ");
					log.info("####"+array[j]);
					log.info(datos.get(array[j]));
					
				}
				muestras = muestras.substring(0, muestras.length()-2);
				muestras = muestras.concat("] ");
			}
			return muestras;
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	private boolean existeValor(int[] array, int valor) {
		boolean existe = false;
		for(int i=0; i<array.length && existe==false; i++) {
			if(array[i]==valor) {
				existe = true;
			}
		}
		return existe;
	}
	
	
	public List<String> getDatos() {
		return datos;
	}

	public void setDatos(List<String> datos) throws Exception {
		try {
			this.datos = datos;
			this.totalDatos = datos.size();
		}catch (Exception e) {
			throw new Exception("Error seteando datos");
		}
	}
	
	public int getTotalDatos() {
		return totalDatos;
	}

	public int getTamanoMuestra() {
		return tamanoMuestra;
	}

	public void setTamanoMuestra(int tamanoMuestra) {
		this.tamanoMuestra = tamanoMuestra;
	}

	public int getNumeroMuestra() {
		return numeroMuestra;
	}

	public void setNumeroMuestra(int numeroMuestra) {
		this.numeroMuestra = numeroMuestra;
	}
	

}
