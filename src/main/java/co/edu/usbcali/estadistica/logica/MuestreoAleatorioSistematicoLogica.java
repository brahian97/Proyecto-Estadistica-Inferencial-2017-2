package co.edu.usbcali.estadistica.logica;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.usbcali.estadistica.utils.Utilidades;
import co.edu.usbcali.estadistica.vista.MuestreoAleatorioSimpleVista;

//http://es.wikihow.com/calcular-la-varianza
public class MuestreoAleatorioSistematicoLogica {
	
	private final static Logger log=LoggerFactory.getLogger(MuestreoAleatorioSistematicoLogica.class);
	
	private List<String> datos = new ArrayList<String>();
	
	private int totalDatos = 0;
	private int tamanoMuestra;
	private int numeroMuestra;
	private int intervalo;
	
	
	public String hallarMuestras() throws Exception {
		try {
			String muestras = "";
			if(tamanoMuestra>totalDatos) {
				throw new Exception("El valor de la muestra no puede ser mayor al total de datos");
			}
			if(tamanoMuestra<0) {
				throw new Exception("El tamaño de la mustra no puede ser menor o igual a 0");
			}
			intervalo = (int) Math.floor(totalDatos/tamanoMuestra);
			final int numRandom = (int) Math.floor(Math.random()*intervalo);
			boolean termino = false;
			int datoSeleccionado = numRandom;
			int cons = 1;
			muestras = muestras.concat("[");
			while(!termino) {
				
				muestras = muestras.concat(datos.get(datoSeleccionado)+", ");
				datoSeleccionado=numRandom + (cons*intervalo);
				cons++;
				log.info("---- dato select --- "+datoSeleccionado);
				if(datoSeleccionado>(totalDatos-1)) {
					termino = true;
				}
			}
			muestras = muestras.substring(0, muestras.length()-2);
			muestras = muestras.concat("] ");	
					
				
			return muestras;
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	private boolean existeValor(int[] array, int valor) {
		boolean existe = false;
		for(int i=0; i<array.length && existe==false; i++) {
			if(array[i]==valor) {
				existe = true;
			}
		}
		return existe;
	}
	
	
	public List<String> getDatos() {
		return datos;
	}

	public void setDatos(List<String> datos) throws Exception {
		try {
			this.datos = datos;
			this.totalDatos = datos.size();
		}catch (Exception e) {
			throw new Exception("Error seteando datos");
		}
	}
	
	public int getTotalDatos() {
		return totalDatos;
	}

	public int getTamanoMuestra() {
		return tamanoMuestra;
	}

	public void setTamanoMuestra(int tamanoMuestra) {
		this.tamanoMuestra = tamanoMuestra;
	}

	public int getNumeroMuestra() {
		return numeroMuestra;
	}

	public void setNumeroMuestra(int numeroMuestra) {
		this.numeroMuestra = numeroMuestra;
	}
	

}
